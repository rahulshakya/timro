import storage from "redux-persist/lib/storage";
import { AllApi, CONTENT_TYPE, createRequestTypes } from "./allApi";
// import Toast from 'react-native-toast-message';
// import * as NavigationService from "../constants/RootNavigation";
// import NodeCache from "node-cache";
// const localStorage = new NodeCache();
import allTypes from "./allTypes";
// import Config from "react-native-config";
/**
 * @function AllActions A function that is used to dispatch actions and also call api using the options in param1
 * @param {function} dispatch
 * @param {object} param1
 * @returns
 */
const getRandomBaseUrl = () => {
  const urls = ["http://localhost:3000/api/"];
  const rand = urls[Math.floor(Math.random() * urls.length)];
  console.log("Used Url", rand);
  return rand;
};
export const AllActions = async (
  dispatch,
  {
    payload = {},
    options = {
      baseUrl: "Config.API_URL",
      method: "post",
      endPoint: "api/app/login",
      types: createRequestTypes("TEST"),
      bearerToken: false,
      onSuccess: false,
      onFailure: false,
      contentType: CONTENT_TYPE.JSON,
      key: "",
    },
  }
) => {
  dispatch({ type: options.types.REQUEST, payload, options });
  try {
    const result = await AllApi({
      options: {
        ...options,
        baseUrl: getRandomBaseUrl(),
      },
      payload: payload,
    });
    // console.log("Response API - " + options.endPoint, result);
    if (result) {
      console.log("API___________", result);
      if (options.onSuccess) {
        options.onSuccess(result);
      }
      dispatch({
        type: options.types.SUCCESS,
        payload: result,
      });
      return result;
    }
  } catch (error) {
    console.log("API______________", error);

    if (error?.response?.status == 401) {
      await sessionStorage.clear();
      // dispatch({
      //   type: allTypes.LOGOUT.SUCCESS,
      // });
      // NavigationService.navigate("More");
    }
    if (error?.message == "Network Error") {
      // Toast.show({
      //   type: 'error',
      //   text1: 'Please check your internet.',
      // });
    }

    // console.log("Error API - " + options.endPoint, error?.response);
    if (error?.response?.data?.message) {
      if (error?.response?.data?.message == "Unauthorized") {
        // Toast.show({
        //   text1: error?.response?.data?.message + ' - Session Expired',
        //   type: 'error',
        // });
        // NavigationService.navigate("Login");
      } else {
        // Toast.show({
        //   text1: error?.response?.data?.message,
        //   type: 'error',
        // });
      }
    }
    if (options.onFailure) {
      options.onFailure(error);
    }
    dispatch({
      type: options.types.FAIL,
      payload: error.response,
    });

    return error.response;
  }
};
