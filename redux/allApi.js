import axios from "axios";
// import NodeCache from "node-cache";
// const localStorage = new NodeCache();
// import AsyncStorage from '@react-native-async-storage/async-storage';
import { isUndefined, isEmpty } from "lodash";
// import { encryptAES } from "../constants/Helper";
// import {navigate} from '../constants/RootNavigation';
axios.defaults.timeout = 30000;

export const CONTENT_TYPE = {
  JSON: "application/json",
  FORM_DATA: "multipart/form-data",
  URL_ENCODED: "application/x-www-form-urlencoded",
};
/**
 * @function HeaderData a functions that saves the bearer token found in login to async storage so that other api can use the same token
 * @param {Enumerator} contentType
 * @param {string} bearerToken
 * @returns {object}
 */
export const HeaderData = async (
  contentType = CONTENT_TYPE.JSON,
  bearerToken = false
) => {
  console.log(await sessionStorage.getItem("token"), 'storage.get("token")');
  const token =
    bearerToken === true || bearerToken === false
      ? await sessionStorage.getItem("token")
      : bearerToken;
  if (token == null) {
    return {
      "Content-Type": contentType,
      // 'Accept-Language': i18n.language,
    };
  } else if (contentType) {
    return {
      Authorization: token,
      "Content-Type": contentType,
      // 'Accept-Language': i18n.language,
    };
  } else {
    return {
      Authorization: token,
      "Content-Type": contentType,
      // 'Accept-Language': i18n.language,
    };
  }
};
/**
 * @function AllApi a functions that can be use to call any api with any method at any end point, with any payload or content-type and bearer token
 * @param {object} action
 * @returns
 */
export const AllApi = async (
  action = {
    options: {
      baseUrl: "http://localhost:3000/",
      method: "post",
      endPoint: "api/app/login",
      types: {},
      bearerToken: false,
      onSuccess: () => console.log("hello"),
      onFailure: () => console.log("adf"),
      contentType: false,
      key: "",
    },
    payload: {},
  }
) => {
  let endPoint = action?.options?.baseUrl + action?.options?.endPoint;
  let response = null;
  if (action?.options?.method == "POST" || action?.options?.method == "post") {
    response = await axios.post(endPoint, action?.payload, {
      headers: await HeaderData(
        action?.options?.contentType,
        action?.options?.bearerToken
      ),
    });
  } else if (
    action?.options?.method == "PUT" ||
    action?.options?.method == "put"
  ) {
    response = await axios.put(endPoint, action?.payload, {
      headers: await HeaderData(
        action?.options?.contentType,
        action?.options?.bearerToken
      ),
    });
  } else if (
    action?.options?.method == "DELETE" ||
    action?.options?.method == "delete"
  ) {
    response = await axios.delete(endPoint, {
      params: { ...action?.options?.params },
      headers: await HeaderData(
        action?.options?.contentType,
        action?.options?.bearerToken
      ),
    });
  } else if (
    action?.options?.method === false &&
    typeof action?.options?.endPoint == "function"
  ) {
    response = await action?.options?.endPoint();
  } else {
    response = await axios.get(endPoint, {
      params: { ...action?.payload },
      headers: await HeaderData(
        action?.options?.contentType,
        action?.options?.bearerToken
      ),
    });
  }
  if (action?.options?.bearerToken === true) {
    if (!isUndefined(response?.data?.meta_data?.access_token)) {
      console.log(
        "_______ASDASDSD_______ASDASDSD",
        response?.data?.meta_data?.access_token
      );
      await sessionStorage.setItem(
        "token",
        response?.data?.meta_data?.access_token
      );
    }
  }
  console.log("_______ASDASDSD", response);

  // if (response?.data?.meta_data?.vi) {
  //   console.log(
  //     action?.options?.key,
  //     response?.data?.data[response?.data?.meta_data?.path],
  //     "response?.data?.data[response?.data?.meta_data?.path]"
  //   );
  //   const res = encryptAES(
  //     response?.data?.data[response?.data?.meta_data?.path],
  //     response?.data?.meta_data?.vi,
  //     action?.options?.key
  //   );
  //   const encryptedPath = {
  //     [response?.data?.meta_data?.path]: JSON.parse(res),
  //   };
  //   console.log(encryptedPath, "path"); // {data:{calc: {...}}}
  //   return {
  //     ...response,
  //     data: {
  //       ...response?.data,
  //       data: {
  //         ...response?.data?.data,
  //         ...encryptedPath,
  //       },
  //     },
  //   };
  // }
  return response;
};
/**
 * @function findAsyncToken can be used to get the token from the storage
 * @returns {Promise<string>}
 */
export const findAsyncToken = async () => {
  const token = await localStorage.get("token");
  if (isEmpty(token)) {
    return false;
  } else {
    return token;
  }
};

/**
 * @function createRequestTypes to create a types that can be standarized for dispatch
 * @param {string} action A value that is to be created into actions
 * @returns {object} object with _request,_success,_fail
 */
export const createRequestTypes = (action) => {
  return {
    REQUEST: `${action}_request`,
    SUCCESS: `${action}_success`,
    FAIL: `${action}_failure`,
  };
};
