import allTypes from "./allTypes";
import { persistReducer } from "redux-persist";
// import storage from '@react-native-async-storage/async-storage';
import storage from "redux-persist/lib/storage";
let initialState = {
  response: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

const persistConfig = {
  key: "getBookmark",
  storage: storage,
};

const NonPersistedReducer = persistReducer(persistConfig, reducer);

export { reducer, NonPersistedReducer };
