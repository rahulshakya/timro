import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { thunk } from "redux-thunk";
import logger from "redux-logger";
import rootReducer from ".";
// import createTransform from "redux-persist/es/createTransform";
// import Flatted from "flatted";
// const rootReducer = combineReducers({
//   blank: (state = null) => state,
// });
// export const transformCircular = createTransform(
//   (inboundState, key) => Flatted.stringify(inboundState),
//   (outboundState, key) => Flatted.parse(outboundState)
// );

// const persistConfig = {
//   key: 'root',
//   storage,
//   blacklist: [],
//   //   transforms: [transformCircular],
// };

// const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = compose;
const middleware = [];
const enhancers = [];

// if (__DEV__) {
//   const createDebugger = require("redux-flipper").default;
//   middleware.push(createDebugger());
middleware.push(logger);
// }
middleware.push(thunk);
enhancers.push(applyMiddleware(...middleware));

const store = createStore(rootReducer, composeEnhancers(...enhancers));
const persistor = persistStore(store);
export { store, persistor };
