import allTypes from "./allTypes";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

let initialState = {
  leadsResponse: {
    data: [],
  },
  usersResponse: {
    data: [],
  },
  loading: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case allTypes.LEADS.SUCCESS:
      return {
        ...state,
        loading: false,
        leadsResponse: action?.payload?.data,
      };
    case allTypes.LEADS.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case allTypes.LEADS.FAIL:
      return {
        ...state,
        loading: false,
      };
    case allTypes.USERS.SUCCESS:
      return {
        ...state,
        loading: false,
        usersResponse: action?.payload?.data,
      };
    case allTypes.USERS.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case allTypes.USERS.FAIL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}

const persistConfig = {
  key: "persistedReducer",
  storage,
};

const PersistedReducer = persistReducer(persistConfig, reducer);

export { reducer, PersistedReducer };
