import { combineReducers } from "redux";
import { PersistedReducer } from "./PersistedReducer";

//import jobCategoryReducer from "../screens//jobsHomeTab/jobCategoryList/Reducer";
// import LoginReducer from '../screens/login/Reducer';
// import NotificationReducer from '../screens/account/notifications/Reducer';
// import TermAndPolicyReducer from '../screens/account/privacyPolicy/Reducer';
const rootReducer = combineReducers({
  blank: (state = null) => state,
  persisted: PersistedReducer,
  // termsAndPrivacy: TermAndPolicyReducer,
});

export default rootReducer;
