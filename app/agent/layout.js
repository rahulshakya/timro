"use client";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import Link from "next/link";
import {
  Bell,
  CircleUser,
  Home,
  Menu,
  Package,
  Package2,
  Search,
  ShoppingCart,
  Users,
} from "lucide-react";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { Badge } from "@/components/ui/badge";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import { Input } from "@/components/ui/input";
import { useRouter } from "next/navigation";
import { ScrollArea } from "@/components/ui/scroll-area";
import { cn } from "@/lib/utils";
import { useEffect, useState } from "react";
import { AllActions } from "@/redux/allAction";
import allTypes from "@/redux/allTypes";
import { CONTENT_TYPE } from "@/redux/allApi";
import { useDispatch } from "react-redux";
import moment from "moment";
import { PopoverAnchor } from "@radix-ui/react-popover";
import { useForm } from "react-hook-form";
import { Label } from "@/components/ui/label";
import { toast } from "@/hooks/use-toast";
import { ToastAction } from "@/components/ui/toast";

export const description =
  "A products dashboard with a sidebar navigation and a main content area. The dashboard has a header with a search input and a user menu. The sidebar has a logo, navigation links, and a card with a call to action. The main content area shows an empty state with a call to action.";

export function ShareCard({ xs = true }) {
  return (
    <Card x-chunk={xs ? "dashboard-02-chunk-0" : undefined}>
      <CardHeader className={xs ? "p-2 pt-0 md:p-4" : undefined}>
        <CardTitle>Share Application</CardTitle>
        <CardDescription>
          To get more leads share our application
        </CardDescription>
      </CardHeader>
      <CardContent className={xs ? "p-2 pt-0 md:p-4 md:pt-0" : undefined}>
        <Button size="sm" className="w-full">
          Share
        </Button>
      </CardContent>
    </Card>
  );
}
export default function RootLayout({ children }) {
  const router = useRouter();
  const actions = useDispatch();
  const [notification, setNotification] = useState({
    data: [],
  });
  const loadNotification = () => {
    AllActions(actions, {
      payload: {},
      options: {
        baseUrl: "http://localhost:3000/api/",
        method: "get",
        endPoint: "notification",
        types: allTypes.NOTIFICATION,
        bearerToken: false,
        onSuccess: (res) => {
          setNotification(res?.data);
        },
        onFailure: () => {},
        contentType: CONTENT_TYPE.JSON,
      },
    });
  };
  useEffect(() => {
    const token = sessionStorage.getItem("token");
    if (!token) {
      alert("Session Expired, Please login");
      router.push("/");
    }
    loadNotification();
  }, []);
  const [showNoti, setShowNoti] = useState(false);
  return (
    <div className="grid min-h-screen w-full md:grid-cols-[220px_1fr] lg:grid-cols-[280px_1fr]">
      <div className="hidden border-r  md:block">
        {/**bg-muted/40 */}
        <div className="flex h-full max-h-screen flex-col gap-2">
          <div className="flex h-14 items-center border-b px-4 lg:h-[60px] lg:px-6">
            <Link href="/" className="flex items-center gap-2 font-semibold">
              <Package2 className="h-6 w-6" />
              <span className="">Acme Inc</span>
            </Link>
            <Popover open={showNoti} onOpenChange={setShowNoti}>
              <PopoverTrigger asChild>
                <Button
                  onClick={() => {
                    loadNotification();
                  }}
                  variant="outline"
                  size="icon"
                  className="ml-auto h-8 w-8"
                >
                  <Bell className="h-4 w-4" />
                  <span className="sr-only">Toggle notifications</span>
                </Button>
              </PopoverTrigger>
              <PopoverContent className="w-80 h-80 overflow-scroll">
                <div className="flex flex-col gap-2 p-4 pt-4">
                  {notification?.data?.map((item) => (
                    <button
                      key={item.id}
                      className={cn(
                        "flex flex-col items-start gap-2 rounded-lg border p-3 text-left text-sm transition-all hover:bg-accent"
                        // mail.selected === item.id && "bg-muted"
                      )}
                      onClick={() => {
                        setShowNoti(false);
                        if (item.type == "LEAD") {
                          router.push("/agent/leads");
                        }
                      }}
                    >
                      <div className="flex w-full flex-col gap-1">
                        <div className="flex items-center">
                          <div className="flex items-center gap-2">
                            <div className="font-semibold">{item.type}</div>
                            {/* {!item.read && (
                                <span className="flex h-2 w-2 rounded-full bg-blue-600" />
                              )} */}
                          </div>
                          <div
                            className={cn(
                              "ml-auto text-xs",
                              // mail.selected === item.id
                              true ? "text-foreground" : "text-muted-foreground"
                            )}
                          >
                            {moment(item?.createdAt).fromNow()}
                          </div>
                        </div>
                        <div className="text-xs font-medium">{item.title}</div>
                      </div>
                      <div className="line-clamp-2 text-xs text-muted-foreground">
                        {item.subtitle.substring(0, 300)}
                      </div>
                      {/* {item.labels.length ? (
                          <div className="flex items-center gap-2">
                            {item.labels.map((label) => (
                              <Badge
                                key={label}
                                variant={"default"}
                                // variant={getBadgeVariantFromLabel(label)}
                              >
                                {label}
                              </Badge>
                            ))}
                          </div>
                        ) : null} */}
                    </button>
                  ))}
                </div>
              </PopoverContent>
            </Popover>
          </div>
          <div className="flex-1">
            <nav className="grid items-start px-2 text-sm font-medium lg:px-4">
              <Link
                href="/agent"
                className="flex items-center gap-3 rounded-lg px-3 py-2 transition-all hover:text-primary" //text-muted-foreground
              >
                <Home className="h-4 w-4" />
                Dashboard
              </Link>
              <Link
                href="/agent/users"
                className="flex items-center gap-3 rounded-lg px-3 py-2 transition-all hover:text-primary" //text-muted-foreground
              >
                <Users className="h-4 w-4" />
                Users
                <Badge className="ml-auto flex h-6 w-6 shrink-0 items-center justify-center rounded-full">
                  6
                </Badge>
              </Link>
              <Link
                href="/agent/leads"
                className="flex items-center gap-3 rounded-lg px-3 py-2 text-primary transition-all hover:text-primary" //bg-muted
              >
                <Package className="h-4 w-4" />
                Leads
              </Link>
              <Link
                href="/agent/plans"
                className="flex items-center gap-3 rounded-lg px-3 py-2 transition-all hover:text-primary" //text-muted-foreground
              >
                <ShoppingCart className="h-4 w-4" />
                Plans
              </Link>
              {/* 
              <Link
                href="#"
                className="flex items-center gap-3 rounded-lg px-3 py-2 transition-all hover:text-primary" //text-muted-foreground
              >
                <LineChart className="h-4 w-4" />
                Analytics
              </Link> */}
            </nav>
          </div>
          <div className="mt-auto p-4">
            <ShareCard xs={true}></ShareCard>
          </div>
        </div>
      </div>
      <div className="flex flex-col">
        <header className="flex h-14 items-center gap-4 border-b  px-4 lg:h-[60px] lg:px-6">
          {/**bg-muted/40 */}
          <Sheet>
            <SheetTrigger asChild>
              <Button
                variant="outline"
                size="icon"
                className="shrink-0 md:hidden"
              >
                <Menu className="h-5 w-5" />
                <span className="sr-only">Toggle navigation menu</span>
              </Button>
            </SheetTrigger>
            <SheetContent side="left" className="flex flex-col">
              <nav className="grid gap-2 text-lg font-medium">
                <Link
                  href="#"
                  className="flex items-center gap-2 text-lg font-semibold"
                >
                  <Package2 className="h-6 w-6" />
                  <span className="sr-only">Acme Inc</span>
                </Link>
                <SheetTrigger asChild>
                  <Link
                    href="/agent"
                    className="mx-[-0.65rem] flex items-center gap-4 rounded-xl px-3 py-2 hover:text-foreground"
                    // text-muted-foreground
                  >
                    <Home className="h-5 w-5" />
                    Dashboard
                  </Link>
                </SheetTrigger>
                <SheetTrigger asChild>
                  <Link
                    href="/agent/users"
                    className="mx-[-0.65rem] flex items-center gap-4 rounded-xl px-3 py-2 hover:text-foreground"
                    //text-muted-foreground
                  >
                    <ShoppingCart className="h-5 w-5" />
                    Users
                    <Badge className="ml-auto flex h-6 w-6 shrink-0 items-center justify-center rounded-full">
                      6
                    </Badge>
                  </Link>
                </SheetTrigger>
                <SheetTrigger asChild>
                  <Link
                    href="/agent/leads"
                    className="mx-[-0.65rem] flex items-center gap-4 rounded-xl px-3 py-2 hover:text-foreground"
                    //text-muted-foreground
                  >
                    <Package className="h-5 w-5" />
                    Leads
                  </Link>
                </SheetTrigger>

                <SheetTrigger asChild>
                  <Link
                    href="#"
                    className="mx-[-0.65rem] flex items-center gap-4 rounded-xl px-3 py-2 hover:text-foreground" //text-muted-foreground
                  >
                    <Users className="h-5 w-5" />
                    Plans
                  </Link>
                </SheetTrigger>
                {/* 
                <SheetTrigger asChild><Link
                  href="#"
                  className="mx-[-0.65rem] flex items-center gap-4 rounded-xl px-3 py-2 hover:text-foreground" //text-muted-foreground
                >
                  <LineChart className="h-5 w-5" />
                  Analytics
                </Link></SheetTrigger> */}
              </nav>
              <div className="mt-auto">
                <ShareCard xs={false} />
              </div>
            </SheetContent>
          </Sheet>
          <div className="w-full flex-1">
            <form>
              <div className="relative">
                <Search className="absolute left-2.5 top-2.5 h-4 w-4" />
                {/* text-muted-foreground */}
                <Input
                  type="search"
                  placeholder="Search products..."
                  className="w-full appearance-none bg-background pl-8 shadow-none md:w-2/3 lg:w-1/3"
                />
              </div>
            </form>
          </div>
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <Button variant="secondary" size="icon" className="rounded-full">
                <CircleUser className="h-5 w-5" />
                <span className="sr-only">Toggle user menu</span>
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent align="end">
              <DropdownMenuSeparator />

              <DropdownMenuItem
                onClick={() => {
                  sessionStorage.clear();
                  router.push("/");
                }}
              >
                Logout
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </header>
        <main className="flex flex-1 flex-col gap-4 p-4 lg:gap-6 lg:p-6">
          <ScrollArea data-state="hide" className="h-lvh">
            {children}
          </ScrollArea>
        </main>
      </div>
    </div>
  );
}

const DropdownMenuItemProfile = () => {
  const [showProfile, setShowProfile] = useState(false);
  const form = useForm({
    defaultValues: {
      username: "",
    },
  });
  return (
    <>
      <DropdownMenuItem
        onClick={(event) => {
          event.preventDefault();
          setShowProfile(true);
        }}
      >
        Profile
      </DropdownMenuItem>
      <Sheet key="top" open={showProfile} onOpenChange={setShowProfile}>
        <SheetContent side="top">
          <SheetHeader>
            <SheetTitle>Update Profile</SheetTitle>
            <SheetDescription></SheetDescription>
          </SheetHeader>
          <div className="flex flex-1 flex-row justify-between">
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="name">Agent Name</Label>
              <Input type="text" id="name" placeholder="Agent Name" />
            </div>
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="authorityCode">Authority Code</Label>
              <Input
                type="text"
                id="authorityCode"
                placeholder="Authority Code"
              />
            </div>
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="contactNumber">Contact Number</Label>
              <Input
                type="mobile"
                id="contactNumber"
                placeholder="Contact Number"
              />
            </div>
          </div>
          <SheetFooter>
            <div className={"flex flex-1 justify-center pt-4"}>
              <Button
                onClick={() => {
                  setShowProfile(false);
                }}
                type="submit"
              >
                Update Profile
              </Button>
            </div>
          </SheetFooter>
        </SheetContent>
      </Sheet>
    </>
  );
};
