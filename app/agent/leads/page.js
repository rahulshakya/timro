"use client";
import {
  ChevronLeft,
  ChevronRight,
  Copy,
  CreditCard,
  File,
  ListFilter,
  MoreHorizontal,
  MoreVertical,
  Truck,
} from "lucide-react";

import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import {
  Pagination,
  PaginationContent,
  PaginationItem,
} from "@/components/ui/pagination";
import { Progress } from "@/components/ui/progress";
import { Separator } from "@/components/ui/separator";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { ScrollArea } from "@/components/ui/scroll-area";
import { useEffect, useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { AllActions } from "@/redux/allAction";
import allTypes from "@/redux/allTypes";
import { CONTENT_TYPE } from "@/redux/allApi";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useRouter } from "next/navigation";
import { Skeleton } from "@/components/ui/skeleton";
import { toast } from "@/hooks/use-toast";
import { ToastAction } from "@/components/ui/toast";

export const description =
  "An products dashboard with a sidebar navigation. The sidebar has icon navigation. The content area has a breadcrumb and search in the header. It displays a list of products in a table with actions.";

export default function Leads() {
  const store = useSelector((state) => {
    return {
      persisted: state.persisted,
    };
  });
  const [leads, setLeads] = useState(store?.persisted?.leadsResponse);
  const actions = useDispatch();
  useEffect(() => {
    AllActions(actions, {
      payload: {},
      options: {
        baseUrl: "http://localhost:3000/api/",
        method: "get",
        endPoint: "leads",
        types: allTypes.LEADS,
        bearerToken: false,
        onSuccess: (res) => {
          setLeads(res?.data);
        },
        onFailure: () => {},
        contentType: CONTENT_TYPE.JSON,
      },
    });
  }, []);
  return (
    <>
      <Tabs defaultValue="all">
        <div className="grid auto-rows-max items-start gap-4 md:gap-8 lg:col-span-2">
          <div className="grid gap-4 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-2 xl:grid-cols-4">
            <Card x-chunk="dashboard-05-chunk-1">
              <CardHeader className="pb-2">
                <CardDescription>This Week</CardDescription>
                <CardTitle className="text-4xl">
                  {leads?.meta_data?.thisWeekCount} Leads
                </CardTitle>
              </CardHeader>
            </Card>
            <Card x-chunk="dashboard-05-chunk-2">
              <CardHeader className="pb-2">
                <CardDescription>This Month</CardDescription>
                <CardTitle className="text-4xl">
                  {leads?.meta_data?.thisMonthCount} Leads
                </CardTitle>
              </CardHeader>
            </Card>
          </div>
          <Tabs defaultValue="week">
            {/* <div className="flex items-center">
              <TabsList>
                <TabsTrigger value="week">Week</TabsTrigger>
                <TabsTrigger value="month">Month</TabsTrigger>
                <TabsTrigger value="year">Year</TabsTrigger>
              </TabsList>
              <div className="ml-auto flex items-center gap-2">
                <DropdownMenu>
                  <DropdownMenuTrigger asChild>
                    <Button
                      variant="outline"
                      size="sm"
                      className="h-7 gap-1 text-sm"
                    >
                      <ListFilter className="h-3.5 w-3.5" />
                      <span className="sr-only sm:not-sr-only">Filter</span>
                    </Button>
                  </DropdownMenuTrigger>
                  <DropdownMenuContent align="end">
                    <DropdownMenuLabel>Filter by</DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuCheckboxItem checked>
                      Fulfilled
                    </DropdownMenuCheckboxItem>
                    <DropdownMenuCheckboxItem>
                      Declined
                    </DropdownMenuCheckboxItem>
                    <DropdownMenuCheckboxItem>
                      Refunded
                    </DropdownMenuCheckboxItem>
                  </DropdownMenuContent>
                </DropdownMenu>
              </div>
            </div> */}
            <TabsContent value="week">
              <Card x-chunk="dashboard-05-chunk-3">
                <CardHeader className="px-7">
                  <CardTitle>Leads</CardTitle>
                  <CardDescription>
                    Recent leads from application.
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHead>Customer</TableHead>
                        <TableHead className=" sm:table-cell">Status</TableHead>
                        <TableHead className="hidden md:table-cell">
                          Created At
                        </TableHead>
                        <TableHead className=" md:table-cell">
                          Actions
                        </TableHead>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {store?.persisted?.loading && (
                        <>
                          <TableRow>
                            <TableCell>
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" sm:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className="hidden md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" sm:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className="hidden md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                          </TableRow>
                        </>
                      )}
                      {leads?.data?.map((v) => (
                        <TableRow>
                          <TableCell>
                            <div className="font-medium">
                              {v?.user?.firstName} {v?.user?.lastName}
                            </div>
                            <div className="text-sm text-muted-foreground md:inline">
                              {v?.plan?.title}
                            </div>
                          </TableCell>

                          <TableCell className=" sm:table-cell">
                            <Badge className="text-xs" variant="secondary">
                              {v?.status}
                            </Badge>
                          </TableCell>
                          <TableCell className="hidden md:table-cell">
                            <div className="font-medium">
                              {moment(v?.updatedAt).format(
                                "YYYY-MM-DD HH:MM:SS"
                              )}
                            </div>
                            <div className="text-sm text-muted-foreground md:inline">
                              (expires{" "}
                              {moment(v?.updatedAt).add(5, "minutes").fromNow()}
                              )
                            </div>
                          </TableCell>
                          <TableCell className=" md:table-cell">
                            {v?.status != "REJECTED" && (
                              <CustomDropDownMenu
                                user={v?.user}
                                history={v}
                              ></CustomDropDownMenu>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            </TabsContent>
          </Tabs>
        </div>
      </Tabs>
    </>
  );
}

const CustomDropDownMenu = ({ user, history }) => {
  const [showDropDown, setShowDropDown] = useState(false);
  return (
    <DropdownMenu open={showDropDown} onOpenChange={setShowDropDown}>
      <DropdownMenuTrigger asChild>
        <Button
          onClick={() => {
            setShowDropDown(true);
          }}
          aria-haspopup="true"
          size="icon"
          variant="ghost"
        >
          <MoreHorizontal className="h-4 w-4" />
          <span className="sr-only">Toggle menu</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuLabel>Actions</DropdownMenuLabel>
        {(history?.status == "CREATED" || history?.status == "TRYABLE") && (
          <DropdownMenuItemWithAlertFollowUp
            user={user}
            history={history}
            onClick={(open) => {
              setShowDropDown(open);
            }}
          ></DropdownMenuItemWithAlertFollowUp>
        )}
        {(history?.status == "IN_PROGRESS" || history?.status == "TRYING") && (
          <DropdownMenuItemWithAlertChangeStatus
            user={user}
            history={history}
            onClick={(open) => {
              setShowDropDown(open);
            }}
          ></DropdownMenuItemWithAlertChangeStatus>
        )}
      </DropdownMenuContent>
    </DropdownMenu>
  );
};

const DropdownMenuItemWithAlertFollowUp = ({
  user = {
    account: "GOOGLE",
    agentId: "2e72b801-c671-429e-9274-90683e5a6457",
    createdAt: "2024-10-03T15:32:16.430Z",
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gt…",
    firstName: "Rahul",
    id: "30f2c8e1-27c1-4c6c-a84f-98033c4a7f4c",
    identifier: "108111783002087858100",
    lastName: "Shakya",
    os: "web",
    otp: "617529",
    password: "Qwerty@123",
    personalDetailsId: "de0eeb38-4212-4f04-a1ec-3a611f191e5e",
    status: "ACTIVATED",
    updatedAt: "2024-10-03T15:36:06.765Z",
  },
  history,
  ...props
}) => {
  const actions = useDispatch();
  const router = useRouter();
  const [alert, setAlert] = useState(false);
  return (
    <AlertDialog {...props} open={alert} onOpenChange={setAlert}>
      <AlertDialogTrigger asChild>
        <DropdownMenuItem
          onClick={(event) => {
            setAlert(true);
            event.preventDefault();
          }}
        >
          Follow Up
        </DropdownMenuItem>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>
            Are you sure you want to follow-up?
          </AlertDialogTitle>
          <AlertDialogDescription>
            You can call at {user?.personalDetails?.mobile} to follow-up with
            customer
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction
            onClick={() => {
              AllActions(actions, {
                payload: {
                  status:
                    history?.status == "TRYABLE" ? "TRYING" : "IN_PROGRESS",
                },
                options: {
                  baseUrl: "http://localhost:3000/api/",
                  method: "put",
                  endPoint: `history/` + history?.id,
                  types: allTypes.HISTORY_LEAD,
                  bearerToken: false,
                  onSuccess: (res) => {
                    toast({
                      title: res?.data?.message,
                      description: "Status changed successfully",
                      action: (
                        <ToastAction altText="Goto schedule to undo">
                          Close
                        </ToastAction>
                      ),
                    });
                    // router.push  ("tel:" + user?.personalDetails?.mobile);
                  },
                  onFailure: (err) => {},
                  contentType: CONTENT_TYPE.JSON,
                },
              });
            }}
          >
            Make A Call
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
const DropdownMenuItemWithAlertChangeStatus = ({
  user = {
    account: "GOOGLE",
    agentId: "2e72b801-c671-429e-9274-90683e5a6457",
    createdAt: "2024-10-03T15:32:16.430Z",
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gt…",
    firstName: "Rahul",
    id: "30f2c8e1-27c1-4c6c-a84f-98033c4a7f4c",
    identifier: "108111783002087858100",
    lastName: "Shakya",
    os: "web",
    otp: "617529",
    password: "Qwerty@123",
    personalDetailsId: "de0eeb38-4212-4f04-a1ec-3a611f191e5e",
    status: "ACTIVATED",
    updatedAt: "2024-10-03T15:36:06.765Z",
  },
  history,
  ...props
}) => {
  const actions = useDispatch();
  const router = useRouter();
  const [alert, setAlert] = useState(false);
  return (
    <AlertDialog {...props} open={alert} onOpenChange={setAlert}>
      <AlertDialogTrigger asChild>
        <DropdownMenuItem
          onClick={(event) => {
            setAlert(true);
            event.preventDefault();
          }}
        >
          Beneficial
        </DropdownMenuItem>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Change Status !!!</AlertDialogTitle>
          <AlertDialogDescription>
            In your opinion, was the lead beneficial for both parties?
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction
            onClick={() => {
              AllActions(actions, {
                payload: {
                  status: "BENEFICIAL",
                },
                options: {
                  baseUrl: "http://localhost:3000/api/",
                  method: "put",
                  endPoint: `history/` + history?.id,
                  types: allTypes.HISTORY_LEAD,
                  bearerToken: false,
                  onSuccess: (res) => {
                    toast({
                      title: res?.data?.message,
                      description: "Status changed successfully",
                      action: (
                        <ToastAction altText="Goto schedule to undo">
                          Close
                        </ToastAction>
                      ),
                    });
                  },
                  onFailure: (err) => {},
                  contentType: CONTENT_TYPE.JSON,
                },
              });
            }}
          >
            Beneficial
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
