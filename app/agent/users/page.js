"use client";
import { ListFilter, MoreHorizontal } from "lucide-react";

import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Progress } from "@/components/ui/progress";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { useEffect, useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { AllActions } from "@/redux/allAction";
import allTypes from "@/redux/allTypes";
import { CONTENT_TYPE } from "@/redux/allApi";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useRouter } from "next/navigation";
import { Skeleton } from "@/components/ui/skeleton";
import { Toast } from "@/components/ui/toast";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
} from "@/components/ui/sheet";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";

export const description =
  "An products dashboard with a sidebar navigation. The sidebar has icon navigation. The content area has a breadcrumb and search in the header. It displays a list of products in a table with actions.";

export default function Users() {
  const store = useSelector((state) => {
    return {
      persisted: state.persisted,
    };
  });
  const [users, setUsers] = useState(store?.persisted?.usersResponse);
  const actions = useDispatch();
  useEffect(() => {
    AllActions(actions, {
      payload: {},
      options: {
        baseUrl: "http://localhost:3000/api/",
        method: "get",
        endPoint: "user",
        types: allTypes.USERS,
        bearerToken: false,
        onSuccess: (res) => {
          setUsers(res?.data);
        },
        onFailure: () => {},
        contentType: CONTENT_TYPE.JSON,
      },
    });
  }, []);
  return (
    <>
      <Tabs defaultValue="all">
        <div className="grid auto-rows-max items-start gap-4 md:gap-8 lg:col-span-2">
          <Tabs defaultValue="week">
            <div className="flex items-center">
              <TabsList>
                <TabsTrigger value="week">Week</TabsTrigger>
                <TabsTrigger value="month">Month</TabsTrigger>
                <TabsTrigger value="year">Year</TabsTrigger>
              </TabsList>
              <div className="ml-auto flex items-center gap-2">
                <DropdownMenu>
                  <DropdownMenuTrigger asChild>
                    <Button
                      variant="outline"
                      size="sm"
                      className="h-7 gap-1 text-sm"
                    >
                      <ListFilter className="h-3.5 w-3.5" />
                      <span className="sr-only sm:not-sr-only">Filter</span>
                    </Button>
                  </DropdownMenuTrigger>
                  <DropdownMenuContent align="end">
                    <DropdownMenuLabel>Filter by</DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuCheckboxItem checked>
                      Fulfilled
                    </DropdownMenuCheckboxItem>
                    <DropdownMenuCheckboxItem>
                      Declined
                    </DropdownMenuCheckboxItem>
                    <DropdownMenuCheckboxItem>
                      Refunded
                    </DropdownMenuCheckboxItem>
                  </DropdownMenuContent>
                </DropdownMenu>
              </div>
            </div>
            <TabsContent value="week">
              <Card x-chunk="dashboard-05-chunk-3">
                <CardHeader className="px-7">
                  <CardTitle>Users</CardTitle>
                  <CardDescription>
                    Recent users from application.
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHead>Customer</TableHead>
                        <TableHead className=" sm:table-cell">Status</TableHead>
                        <TableHead className="hidden md:table-cell">
                          Created At
                        </TableHead>
                        <TableHead className=" md:table-cell">
                          Actions
                        </TableHead>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {store?.persisted?.loading && (
                        <>
                          <TableRow>
                            <TableCell>
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" sm:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className="hidden md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                          </TableRow>

                          <TableRow>
                            <TableCell>
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" sm:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className="hidden md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                          </TableRow>

                          <TableRow>
                            <TableCell>
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" sm:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className="hidden md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                            <TableCell className=" md:table-cell">
                              <Skeleton className="h-4 w-full" />
                            </TableCell>
                          </TableRow>
                        </>
                      )}
                      {users?.data?.map((v) => (
                        <TableRow>
                          <TableCell>
                            <div className="font-medium">
                              {v?.firstName} {v?.lastName}
                            </div>
                            <div className="text-sm text-muted-foreground md:inline">
                              {v?.email}
                            </div>
                          </TableCell>

                          <TableCell className=" sm:table-cell">
                            <Badge className="text-xs" variant="secondary">
                              {v?.status}
                            </Badge>
                          </TableCell>
                          <TableCell className="hidden md:table-cell">
                            <div className="font-medium">
                              {moment(v?.createdAt).format("YYYY-MM-DD")}
                            </div>
                            <div className="text-sm text-muted-foreground md:inline">
                              ({v?.os})
                            </div>
                          </TableCell>
                          <TableCell className=" md:table-cell">
                            <CustomDropDownMenu user={v}></CustomDropDownMenu>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            </TabsContent>
          </Tabs>
        </div>
      </Tabs>
    </>
  );
}

const CustomDropDownMenu = ({ user }) => {
  const [showDropDown, setShowDropDown] = useState(false);
  return (
    <DropdownMenu open={showDropDown}>
      <DropdownMenuTrigger asChild>
        <Button
          onClick={() => {
            setShowDropDown(true);
          }}
          aria-haspopup="true"
          size="icon"
          variant="ghost"
        >
          <MoreHorizontal className="h-4 w-4" />
          <span className="sr-only">Toggle menu</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuLabel>Actions</DropdownMenuLabel>
        {(user?.status == "CONTRIBUTOR" || user?.status == "AGENT") && (
          <DropdownMenuItemWithAlertMakeAgent
            user={user}
            onOpenChange={(open) => {
              setShowDropDown(open);
            }}
          ></DropdownMenuItemWithAlertMakeAgent>
        )}
        {!(
          user?.status == "CONTRIBUTOR" || user?.status == "ADMINISTRATOR"
        ) && (
          <DropdownMenuItemWithAlertMakeContributor
            user={user}
            onOpenChange={(open) => {
              setShowDropDown(open);
            }}
          ></DropdownMenuItemWithAlertMakeContributor>
        )}

        <DropdownMenuItem
          onClick={() => {
            setShowDropDown(false);
          }}
        >
          Cancel
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};

const DropdownMenuItemWithAlertMakeAgent = ({
  user = {
    account: "GOOGLE",
    agentId: "2e72b801-c671-429e-9274-90683e5a6457",
    createdAt: "2024-10-03T15:32:16.430Z",
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gt…",
    firstName: "Rahul",
    id: "30f2c8e1-27c1-4c6c-a84f-98033c4a7f4c",
    identifier: "108111783002087858100",
    lastName: "Shakya",
    os: "web",
    otp: "617529",
    password: "Qwerty@123",
    personalDetailsId: "de0eeb38-4212-4f04-a1ec-3a611f191e5e",
    status: "ACTIVATED",
    updatedAt: "2024-10-03T15:36:06.765Z",
  },
  ...props
}) => {
  const router = useRouter();
  const actions = useDispatch();
  const [showProfile, setShowProfile] = useState(false);
  const [formData, setFormData] = useState({
    role: "AGENT",
    user_id: user?.id,
    agentName: "Rahul Shakya",
    authorityCode: "30006455",
    contactNumber: "9866429659",
    ...user?.agent,
  });
  return (
    <>
      <DropdownMenuItem
        onClick={(event) => {
          event.preventDefault();
          setShowProfile(true);
        }}
      >
        Make Agent
      </DropdownMenuItem>
      <Sheet key="top" open={showProfile} onOpenChange={setShowProfile}>
        <SheetContent side="top">
          <SheetHeader>
            <SheetTitle>Make Agent</SheetTitle>
            <SheetDescription></SheetDescription>
          </SheetHeader>
          <div className="flex flex-1 flex-row justify-between">
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="name">Agent Name</Label>
              <Input
                value={formData.agentName}
                onChange={(event) =>
                  setFormData({
                    ...formData,
                    agentName: event?.target?.value,
                  })
                }
                type="text"
                id="name"
                placeholder="Agent Name"
              />
            </div>
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="authorityCode">Authority Code</Label>
              <Input
                value={formData.authorityCode}
                onChange={(event) =>
                  setFormData({
                    ...formData,
                    authorityCode: event?.target?.value,
                  })
                }
                type="text"
                id="authorityCode"
                placeholder="Authority Code"
              />
            </div>
            <div className="grid w-full max-w-sm items-center gap-1.5">
              <Label htmlFor="contactNumber">Contact Number</Label>
              <Input
                value={formData.contactNumber}
                onChange={(event) =>
                  setFormData({
                    ...formData,
                    contactNumber: event?.target?.value,
                  })
                }
                type="mobile"
                id="contactNumber"
                placeholder="Contact Number"
              />
            </div>
          </div>
          <SheetFooter>
            <div className={"flex flex-1 justify-center pt-4"}>
              <Button
                onClick={() => {
                  AllActions(actions, {
                    payload: formData,
                    options: {
                      baseUrl: "http://localhost:3000/api/",
                      method: "put",
                      endPoint: `user`,
                      types: allTypes.MAKE_AGENT,
                      bearerToken: false,
                      onSuccess: (res) => {
                        setShowProfile(false);
                      },
                      onFailure: (err) => {},
                      contentType: CONTENT_TYPE.JSON,
                    },
                  });
                }}
                type="submit"
              >
                Update Agent
              </Button>
            </div>
          </SheetFooter>
        </SheetContent>
      </Sheet>
    </>
  );
};

const DropdownMenuItemWithAlertMakeContributor = ({
  user = {
    id: "2e72b801-c671-429e-9274-90683e5a6457",
    createdAt: "2024-10-03T15:32:16.430Z",
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gt…",
    firstName: "Rahul",
    id: "30f2c8e1-27c1-4c6c-a84f-98033c4a7f4c",
    identifier: "108111783002087858100",
    lastName: "Shakya",
    os: "web",
    otp: "617529",
    password: "Qwerty@123",
    personalDetailsId: "de0eeb38-4212-4f04-a1ec-3a611f191e5e",
    status: "ACTIVATED",
    updatedAt: "2024-10-03T15:36:06.765Z",
  },
  ...props
}) => {
  const router = useRouter();
  const actions = useDispatch();
  return (
    <AlertDialog {...props}>
      {user.status != "CONTRIBUTOR" && (
        <AlertDialogTrigger asChild>
          <DropdownMenuItem>Make Contributor</DropdownMenuItem>
        </AlertDialogTrigger>
      )}
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you sure?</AlertDialogTitle>
          <AlertDialogDescription>
            Do you demote {user?.email} contributor?
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction
            onClick={() => {
              AllActions(actions, {
                payload: {
                  role: "CONTRIBUTOR",
                  user_id: user?.id,
                },
                options: {
                  baseUrl: "http://localhost:3000/api/",
                  method: "put",
                  endPoint: `user`,
                  types: allTypes.MAKE_CONTRIBUTOR,
                  bearerToken: false,
                  onSuccess: (res) => {},
                  onFailure: (err) => {},
                  contentType: CONTENT_TYPE.JSON,
                },
              });
            }}
          >
            Make Contributor
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
