"use client";
import { ListFilter, MoreHorizontal } from "lucide-react";

import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Progress } from "@/components/ui/progress";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { useEffect, useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { AllActions } from "@/redux/allAction";
import allTypes from "@/redux/allTypes";
import { CONTENT_TYPE } from "@/redux/allApi";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useRouter } from "next/navigation";
import { Skeleton } from "@/components/ui/skeleton";

export const description =
  "An products dashboard with a sidebar navigation. The sidebar has icon navigation. The content area has a breadcrumb and search in the header. It displays a list of products in a table with actions.";

export default function Plans() {
  const store = useSelector((state) => {
    return {
      persisted: state.persisted,
    };
  });
  const [plans, setPlans] = useState(store?.persisted?.plansResponse);
  const actions = useDispatch();
  useEffect(() => {
    AllActions(actions, {
      payload: {},
      options: {
        baseUrl: "http://localhost:3000/api/",
        method: "get",
        endPoint: "companies",
        types: allTypes.PLANS,
        bearerToken: false,
        onSuccess: (res) => {
          setPlans(res?.data);
        },
        onFailure: () => {},
        contentType: CONTENT_TYPE.JSON,
      },
    });
  }, []);
  return (
    <>
      <Tabs defaultValue="all">
        <div className="grid auto-rows-max items-start gap-4 md:gap-8 lg:col-span-2">
          <Tabs defaultValue="week">
            <div className="flex items-center">
              <div className="ml-auto flex items-center gap-2">
                <DropdownMenu>
                  <DropdownMenuTrigger asChild>
                    <Button
                      variant="outline"
                      size="sm"
                      className="h-7 gap-1 text-sm"
                    >
                      <ListFilter className="h-3.5 w-3.5" />
                      <span className="sr-only sm:not-sr-only">Filter</span>
                    </Button>
                  </DropdownMenuTrigger>
                  <DropdownMenuContent align="end">
                    <DropdownMenuLabel>Filter by</DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuCheckboxItem checked>
                      Active
                    </DropdownMenuCheckboxItem>
                    <DropdownMenuCheckboxItem>Drafted</DropdownMenuCheckboxItem>
                  </DropdownMenuContent>
                </DropdownMenu>
              </div>
            </div>
            <TabsContent value="week">
              <Card x-chunk="dashboard-05-chunk-3">
                <CardHeader className="px-7">
                  <CardTitle>Plans</CardTitle>
                  <CardDescription>
                    Recent Plans from application.
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <Accordion type="single" collapsible className="w-full">
                    {plans?.data?.map((v) => (
                      <AccordionItem value={"item-company-" + v.id}>
                        <AccordionTrigger>{v?.name}</AccordionTrigger>
                        <AccordionContent>
                          <Accordion
                            type="single"
                            collapsible
                            className="w-full"
                          >
                            {v.plans.map((k) => (
                              <AccordionItem value={"item-plan-" + k?.id}>
                                <AccordionTrigger>{k?.title}</AccordionTrigger>
                                <AccordionContent>
                                  <div className="flex flex-1">
                                    <Table>
                                      <TableBody>
                                        {k?.inputs.map((l) => (
                                          <TableRow>
                                            <TableCell>{l?.title}</TableCell>
                                          </TableRow>
                                        ))}
                                      </TableBody>
                                    </Table>
                                    <Table>
                                      <TableBody>
                                        {k?.reports.map((l) => (
                                          <TableRow>
                                            <TableCell>{l?.title}</TableCell>
                                          </TableRow>
                                        ))}
                                      </TableBody>
                                    </Table>
                                  </div>
                                </AccordionContent>
                              </AccordionItem>
                            ))}
                          </Accordion>
                        </AccordionContent>
                      </AccordionItem>
                    ))}
                  </Accordion>
                </CardContent>
              </Card>
            </TabsContent>
          </Tabs>
        </div>
      </Tabs>
    </>
  );
}

const CustomDropDownMenu = ({ user }) => {
  const [showDropDown, setShowDropDown] = useState(false);
  return (
    <DropdownMenu open={showDropDown}>
      <DropdownMenuTrigger asChild>
        <Button
          onClick={() => {
            setShowDropDown(true);
          }}
          aria-haspopup="true"
          size="icon"
          variant="ghost"
        >
          <MoreHorizontal className="h-4 w-4" />
          <span className="sr-only">Toggle menu</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuLabel>Actions</DropdownMenuLabel>
        <DropdownMenuItemWithAlertMakeAgent
          user={user}
          onOpenChange={(open) => {
            setShowDropDown(open);
          }}
        ></DropdownMenuItemWithAlertMakeAgent>

        <DropdownMenuItem
          onClick={() => {
            setShowDropDown(false);
          }}
        >
          Cancel
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};

const DropdownMenuItemWithAlertMakeAgent = ({
  user = {
    account: "GOOGLE",
    agentId: "2e72b801-c671-429e-9274-90683e5a6457",
    createdAt: "2024-10-03T15:32:16.430Z",
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gt…",
    firstName: "Rahul",
    id: "30f2c8e1-27c1-4c6c-a84f-98033c4a7f4c",
    identifier: "108111783002087858100",
    lastName: "Shakya",
    os: "web",
    otp: "617529",
    password: "Qwerty@123",
    personalDetailsId: "de0eeb38-4212-4f04-a1ec-3a611f191e5e",
    status: "ACTIVATED",
    updatedAt: "2024-10-03T15:36:06.765Z",
  },
  ...props
}) => {
  const router = useRouter();
  return (
    <AlertDialog {...props}>
      {!user.agentId && (
        <AlertDialogTrigger asChild>
          <DropdownMenuItem>Make Agent</DropdownMenuItem>
        </AlertDialogTrigger>
      )}
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you sure?</AlertDialogTitle>
          <AlertDialogDescription>
            Do you have all the necessary document signed to make {user?.email}{" "}
            agent?
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={() => {}}>Make Agent</AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};
