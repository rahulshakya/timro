import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import nodemailer from "nodemailer";
const generateOTP = () => {
  return Math.floor(100000 + Math.random() * 900000).toString(); // Generates a random 6-digit OTP
};
export async function POST(request, response) {
  const prisma = new PrismaClient();
  const body = await request.json();
  let user = null;
  if (body?.email) {
    user = await prisma.user.findFirst({
      where: {
        email: body?.email,
        OR: [{ status: "AGENT" }, { status: "ADMINISTRATOR" }],
      },
      include: { agent: true },
    });
    if (user) {
      let otp = generateOTP();

      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "rahulsaqya@gmail.com", // Your email
          pass: "rddr lqmb tavy uoiy", // Your email password
        },
      });

      // Send OTP email
      try {
        await prisma.user.update({
          data: {
            otp: otp,
          },
          where: {
            id: user?.id,
            email: body?.email,
          },
        });
        await transporter.sendMail({
          from: '"Your CalcV" <rahulsaqya@gmail.com>', // Sender address
          to: "aggroholder@gmail.com", // List of recipients
          subject: "Your OTP Code", // Subject line
          text: `Your OTP code is ${otp}`, // Plain text body
        });

        return NextResponse.json(
          {
            data: user,
            message: "OTP was sent successfully",
            meta_data: {
              otp: otp,
            },
          },
          { status: 200 }
        );
      } catch (error) {
        console.log(error);
        return NextResponse.json(
          {
            data: "",
            message: "Error Sending OTP",
            meta_data: {},
          },
          { status: 500 }
        );
      }
    } else {
      return NextResponse.json(
        {
          data: "",
          message: "User not found, Please register using app",
          meta_data: {},
        },
        { status: 400 }
      );
    }
  } else {
    return NextResponse.json(
      {
        data: "",
        message: "Email is required",
        meta_data: {},
      },
      { status: 400 }
    );
  }
}
