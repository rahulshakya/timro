import { signJwtAccessToken, verifyJwt } from "@/lib/jwt";
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { include_personalDetails } from "../form/address/route";
export async function GET(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  // const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  try {
    if (accessToken && user) {
      const user = await prisma.user.findMany({
        include: {
          agent: true,
        },
      });

      return NextResponse.json({
        data: user,
        message: "Leads listed successfully",
        meta_data: {},
      });
    } else {
      return NextResponse.json(
        {
          message: "Un-authorized",
          data: "Not authorized, please recheck your email and password",
          meta_data: {},
        },
        { status: 401 }
      );
    }
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { message: "Error", error: error.message },
      { status: 500 }
    );
  }
}

export async function POST(request, response) {
  const prisma = new PrismaClient();
  const body = await request.json();
  let user = null;
  if (body?.password) {
    user = await prisma.user.findFirst({
      where: {
        email: body?.os != "ios" ? body?.email : undefined,
        identifier: body?.identifier,
        password: body?.password,
        status: {
          not: "DELETED",
        },
      },
      include: {
        agent: true,
        personalDetails: { include: include_personalDetails },
      },
    });
    console.log(user, "useruser");
    let accessToken = null;
    if (user) {
      const { identifier, ...jwtUser } = user;
      accessToken = signJwtAccessToken(jwtUser);
    }
    return NextResponse.json(
      {
        data: !user
          ? "Not authorized, please recheck your email and password"
          : user,
        message: !user ? "Un-authorized" : "Login Successfully",
        meta_data: {
          access_token: accessToken,
        },
      },
      { status: !user ? 401 : 200 }
    );
  } else {
    user = await prisma.user.findFirst({
      where: {
        email: body?.os != "ios" ? body?.email : undefined,
        identifier: body?.identifier,
        status: {
          not: "DELETED",
        },
      },
      include: {
        agent: true,
        personalDetails: { include: include_personalDetails },
      },
    });
  }

  if (user) {
    const { identifier, ...jwtUser } = user;
    const accessToken = signJwtAccessToken(jwtUser);
    try {
      if (body?.os != "ios") {
        await prisma.user.update({
          data: {
            firstName: body?.firstName,
            lastName: body?.lastName,
            fcm: body?.fcm,
            os: body?.os,
          },
          where: {
            id: user.id,
          },
        });
      } else {
        console.log(body?.os != "ios");

        await prisma.user.update({
          data: {
            fcm: body?.fcm,
            os: body?.os,
          },
          where: {
            id: user.id,
          },
        });
      }
    } catch (e) {
      console.log("Failed to update user");
    }
    return NextResponse.json(
      {
        data: !user
          ? "Not authorized, please recheck your email and password"
          : user,
        message: !user ? "Un-authorized" : "Login Successfully",
        meta_data: {
          access_token: accessToken,
        },
      },
      { status: !user ? 401 : 200 }
    );
  } else {
    console.log(body, "bodybody");
    const personalDetails = await prisma.personalDetails.create({
      data: {
        email: body?.email,
      },
    });
    // checkifvalidgoogle/apple user
    const user = await prisma.user.create({
      data: {
        email: body?.email,
        identifier: body?.identifier,
        firstName: body?.firstName,
        lastName: body?.lastName,
        fcm: body?.fcm,
        os: body?.os,
        personalDetailsId: personalDetails.id,
      },
      include: { personalDetails: true },
    });
    const { identifier, ...jwtUser } = user;
    const accessToken = signJwtAccessToken(jwtUser);

    return NextResponse.json({
      data: user,
      message: "User created successfully",
      meta_data: {
        access_token: accessToken,
      },
    });
  }
}

export async function DELETE(request, response) {
  // Need JWT Token
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log(user, accessToken, "__USER");

  if (accessToken && user) {
    // : { params: { company_id: number; plan_id: number } }
    // : { params: { company_id: number } }
    const prisma = new PrismaClient();
    const data = await prisma.user.findFirst({
      where: {
        id: user.id,
      },
    });
    const updatedUser = await prisma.user.update({
      data: {
        ...data,
        status: "DELETED",
      },
      where: {
        id: data.id,
      },
      include: {
        agent: true,
        personalDetails: { include: include_personalDetails },
      },
    });
    return NextResponse.json({
      message: "User deleted successfully",
      data: updatedUser,
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}

export async function PUT(request, response) {
  // Need JWT Token
  const accessToken = request.headers.get("Authorization");
  const body = await request.json();
  const user = verifyJwt(accessToken);
  console.log(user, accessToken, "__USER");

  if (accessToken && user) {
    // : { params: { company_id: number; plan_id: number } }
    // : { params: { company_id: number } }
    const prisma = new PrismaClient();
    const data = await prisma.user.findFirst({
      where: {
        id: body?.user_id,
      },
    });
    const role = () => {
      switch (body?.role) {
        case "CONTRIBUTOR":
          return "CONTRIBUTOR";
        case "AGENT":
          return "AGENT";
        default:
          return "ACTIVATED";
      }
    };
    let agentUser = null;
    if (role() == "AGENT") {
      /**
       *
       * agentName,authorityCode,contactNumber,user_id
       * API for administrator so that they can update necessary data about agent
       */

      if (user?.status == "ADMINISTRATOR") {
        let agent = null;
        ``;
        if (data.agentId) {
          agent = await prisma.agent.update({
            data: {
              agentName: body?.agentName,
              authorityCode: body?.authorityCode,
              contactNumber: body?.contactNumber,
            },
            where: {
              id: data.agentId,
            },
          });
        } else {
          agent = await prisma.agent.create({
            data: {
              agentName: body?.agentName,
              authorityCode: body?.authorityCode,
              contactNumber: body?.contactNumber,
            },
          });
        }
        agentUser = await prisma.user.update({
          data: {
            status: role(),
            agentId: agent.id,
          },
          where: {
            id: body?.user_id,
          },
        });
      } else {
        return NextResponse.json(
          {
            message: "Un-authorized",
            data: "Not authorized, please recheck your email and password",
            meta_data: {},
          },
          { status: 401 }
        );
      }

      await prisma.notification.create({
        data: {
          title: `Congratulation, You are verified - [${role()}]`,
          subtitle: `Hi ${agentUser?.firstName} ${agentUser?.lastName}, You are now a verified agent`,
          type: `AGENT`,
          type_id: agentUser?.id,
          createdId: agentUser?.id,
        },
      });
      return NextResponse.json({
        message: "User upgraded successfully",
        data: agentUser,
        meta_data: {
          subtitle:
            "You will receive email mentioning the ways you can contribute to help us grow",
        },
      });
    }
    const updatedUser = await prisma.user.update({
      data: {
        ...data,
        status: role(),
      },
      where: {
        id: body?.user_id,
      },
      include: {
        agent: true,
        personalDetails: { include: include_personalDetails },
      },
    });
    return NextResponse.json({
      message: "User upgraded successfully",
      data: updatedUser,
      meta_data: {
        subtitle:
          "You will receive email mentioning the ways you can contribute to help us grow",
      },
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
