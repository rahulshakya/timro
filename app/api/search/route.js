import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
export async function GET(
  request,
  // : Request
  { params }
) {
  const keyword = request.nextUrl.searchParams.get("s");
  console.log(keyword, "_keyword");
  // : { params: { company_id: number } }
  const prisma = new PrismaClient();
  const plans = await prisma.plan.findMany({
    where: {
      OR: [
        { title: { contains: keyword } },
        { detail: { contains: keyword } },
        { website: { contains: keyword } },
        { contact: { contains: keyword } },
        { email: { contains: keyword } },
      ],
    },
    include: { category: true, company: true },
  });
  const companies = await prisma.company.findMany({
    where: {
      OR: [
        { name: { contains: keyword } },
        { detail: { contains: keyword } },
        { website: { contains: keyword } },
        { contact: { contains: keyword } },
        { email: { contains: keyword } },
        { location: { contains: keyword } },
      ],
    },
  });

  return NextResponse.json({
    message: "Data fetched successfully",
    data: [...plans, ...companies],
    meta_data: { keyword: keyword },
  });
}
