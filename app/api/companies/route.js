import { NextResponse } from "next/server";
import { dataTypos } from "../plans/[company_id]/route";
import { useSearchParams } from "next/navigation";
const { PrismaClient } = require("@prisma/client");
export async function GET(request) {
  const searchParams = new URLSearchParams(request.url);

  const itemsPerPage = searchParams.get("per_page") || 100;
  const pageNo = searchParams.get("page") || 1;
  const skip = (pageNo - 1) * itemsPerPage;

  const prisma = new PrismaClient();
  const total = await prisma.company.count();
  const data = await prisma.company.findMany({
    take: parseInt(itemsPerPage),
    skip,
    orderBy: { createdAt: "desc" }, // Adjust as needed
    // TODO only if authorized and is agent true and admin
    include: { plans: { include: { inputs: true, reports: true } } },
  });
  const last_updated = await prisma.company.findFirst({
    orderBy: { updatedAt: "desc" },
  });
  return NextResponse.json({
    message: "Companies fetched successfully",
    data: data,
    meta_data: {
      page: pageNo,
      has_next: total - pageNo * itemsPerPage > 0 ? true : false,
      total: total,
      per_page: itemsPerPage,
      last_updated: last_updated.updatedAt,
    },
  });
}
