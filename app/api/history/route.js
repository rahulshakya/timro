import { verifyJwt } from "@/lib/jwt";
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
export async function GET(
  request,
  // : Request
  { params }
) {
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    const prisma = new PrismaClient();
    const data = await prisma.history.findMany({
      orderBy: {
        createdAt: "desc",
      },
    });

    return NextResponse.json({
      message: "History fetched successfully",
      data: data,
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
