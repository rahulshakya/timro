import { verifyJwt } from "@/lib/jwt";
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
export async function PUT(
  request,
  // : Request
  { params }
) {
  const accessToken = request.headers.get("Authorization");
  const body = await request.json();
  const user = verifyJwt(accessToken);
  const status = () => {
    switch (body?.status) {
      case "CREATED":
        return body?.status;
      case "IN_PROGRESS":
        return body?.status;
      case "TRYING":
        return body?.status;
      case "BENEFICIAL":
        return body?.status;
      default:
        return "NULL";
    }
  };
  if (accessToken && user) {
    const prisma = new PrismaClient();
    const data = await prisma.history.update({
      data: {
        status: status(),
      },
      where: {
        id: params?.id,
      },
      include: {
        user: {
          include: {
            personalDetails: {
              include: {
                agent: {
                  include: {
                    userAgent: true,
                  },
                },
              },
            },
          },
        },
      },
    });
    if (body?.status == "CREATED" || body?.status == "TRYABLE") {
      await prisma.notification.create({
        data: {
          title: `You got a new lead - [${status()}]`,
          subtitle: `${data?.user?.firstName} ${data?.user?.lastName} might be a potential client`,
          type: `LEAD`,
          type_id: data?.id,
          createdId: data?.user?.personalDetails?.agent?.userAgent?.id,
        },
      });
    }

    return NextResponse.json({
      message: "History updated successfully",
      data: data,
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
