import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
// export type dataTypos = {
//   id: number;
//   name: string;
//   logo: string;
//   website: string;
//   contact: string;
//   email: string;
//   detail: string;
//   plan: Array<{
//     id: number;
//     name: string;

//     category: { id: number; name: string };
//     image: string;
//     website: string;
//     contact: string;
//     email: string;
//     calculate: {
//       id: number;
//       formulae: string;
//       variables: any;
//     };
//     input: Array<{
//       id: number;
//       name: string;
//       identifier: string;
//       validity_message: string;
//       class_validators: string;
//       type: string;
//       options: Array<{
//         id: number;
//         series: number;
//         value: any;
//         name: string;
//       }>;
//       series: number;
//       selected: any;
//     }>;
//     report: Array<{id: number,title: String,  formulae: String}>
//   }>;
// };
export async function GET(
  request,
  // : Request
  { params }
) {
  // : { params: { company_id: number } }
  const prisma = new PrismaClient();
  const data = await prisma.company.findFirst({
    orderBy: { updatedAt: "desc" },
  });

  return NextResponse.json({
    message: "Data fetched successfully",
    data: {
      contact_email: "rahulsaqya@gmail.com",
      contact_mobile: "+9779866429659",
      terms_and_condition_url: "https://depremium.tumblr.com/terms-conditions",
      privacy_policy_url: "https://depremium.tumblr.com/terms-conditions",
    },
    meta_data: {
      company: data.updatedAt,
    },
  });
}
