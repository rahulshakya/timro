import { NextResponse } from "next/server";
import { data } from "../../companies/route";
import { PrismaClient } from "@prisma/client";
// export type dataTypos = {
//   id: number;
//   name: string;
//   logo: string;
//   website: string;
//   contact: string;
//   email: string;
//   detail: string;
//   plan: Array<{
//     id: number;
//     name: string;

//     category: { id: number; name: string };
//     image: string;
//     website: string;
//     contact: string;
//     email: string;
//     calculate: {
//       id: number;
//       formulae: string;
//       variables: any;
//     };
//     input: Array<{
//       id: number;
//       name: string;
//       identifier: string;
//       validity_message: string;
//       class_validators: string;
//       type: string;
//       options: Array<{
//         id: number;
//         series: number;
//         value: any;
//         name: string;
//       }>;
//       series: number;
//       selected: any;
//     }>;
//     report: Array<{id: number,title: String,  formulae: String}>
//   }>;
// };
export async function GET(
  request,
  // : Request
  { params }
) {
  // : { params: { company_id: number } }
  const prisma = new PrismaClient();
  const data = await prisma.plan.findMany({
    where: {
      companyId: params.company_id,
    },
    include: { category: true, company: true },
  });
  const company = data[0].company;

  return NextResponse.json({
    message: "Plan fetched successfully",
    data: data.map((v) => {
      return { ...v, company: undefined };
    }),
    meta_data: { company },
  });
}
