import { verifyJwt } from "@/lib/jwt";
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
import { include_personalDetails } from "../../form/address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const calculatorGenerated = await request.json();
  // Need JWT Token
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    // : { params: { company_id: number; plan_id: number } }
    // : { params: { company_id: number } }
    const prisma = new PrismaClient();
    const data = await prisma.plan.findFirst({
      where: {
        id: params?.plan_id,
      },
      include: { reports: true, calculate: true },
    });
    // const createHistory = await prisma.history.create({
    //   data: {
    //     planId: params?.plan_id,
    //     userId: user.id,
    //     generated: "",
    //     calculator: "",
    //   },
    // });
    let history = null;
    console.log(user, "----user");
    history = await prisma.history.create({
      data: {
        generated: JSON.stringify(calculatorGenerated),
        calculator: data.calculate.formulae,
        planId: data.id,
        userId: user.id,
      },
      include: {
        user: {
          include: {
            personalDetails: { include: { agent: true } },
            agent: { include: { userAgent: true } },
          },
        },
        plan: { include: { forms: true } },
      },
    });

    const theVariables = calculatorGenerated;
    const reports = data.reports?.map((v) => {
      v.formulae = calculateFormulae(v.formulae, theVariables);
      return v;
    });
    return NextResponse.json({
      message: "Report fetched successfully",
      data: reports,
      meta_data: { history },
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
export const calculateFormulae = (formula, values) => {
  try {
    const variableNames = Object.keys(values);
    const variableValues = variableNames.map((name) => values[name]);
    const formulaFunction = new Function(...variableNames, `return ${formula}`);
    const result = formulaFunction(...variableValues);
    return result;
  } catch (e) {
    console.log("___", e);
  }
};
