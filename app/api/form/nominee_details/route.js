import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log("datat", user);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const nomineeDetail = await prisma.nomineeDetail.create({
    data: {
      name: body.name,
      surname: body.surname,
      nameInNepali: body.nameInNepali,
      surnameInNepali: body.surnameInNepali,
      relationWithInsured: body.relationWithInsured,
    },
  });

  const personalDetails = await prisma.personalDetails.update({
    data: {
      nomineeDetailId: nomineeDetail.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
