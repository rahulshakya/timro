import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log("datat", user);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const physicalMakeup = await prisma.physicalMakeUp.create({
    data: {
      yourHeight: body.yourHeight,
      yourWeight: body.yourWeight,
      drinkAlcohol: body.drinkAlcohol,
      smoke: body.smoke,
      use_drug: body.use_drug,
    },
  });

  const personalDetails = await prisma.personalDetails.update({
    data: {
      physicalMakeUpId: physicalMakeup.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
