import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log("datat", user);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const familyDetail = await prisma.familyDetail.create({
    data: {
      spouse: body.spouse,
      father: body.father,
      mother: body.mother,
      grandFather: body.grandFather,
      son: body.son,
      daughter: body.daughter,
      daughterInLaw: body.daughterInLaw,
      fatherInLaw: body.fatherInLaw,
    },
  });

  const personalDetails = await prisma.personalDetails.update({
    data: {
      familyDetailId: familyDetail.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
