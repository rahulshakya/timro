import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import fs from "fs";
import path from "path";
import fetch from "node-fetch";
import moment from "moment";
import { readdir } from "fs/promises";
export async function GET(
  request,
  // : Request
  { params }
) {
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    const prisma = new PrismaClient();
    const districtList = await prisma.district.findMany();

    return NextResponse.json({
      data: districtList,
      message: "Get Districts Successfully",
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
export const include_personalDetails = {
  permanentAddress: { include: { district: true } },
  currentAddress: { include: { district: true } },
  familyDetail: true,
  incomeStatement: { include: { panDoc: true } },
  nomineeDetail: true,
  intendJob: true,
  healthDetail: true,
  agent: { include: { userAgent: true } },
  physicalMakeUp: true,
  familyHistory: true,
  govtDocument: true,
  aml: true,
  bank: true,
};
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log(user, "asdfiahdkjfahkn");
  let personalDetails = await prisma.personalDetails.findFirst({
    where: {
      id: user.personalDetailsId,
    },
  });

  let permanentAddress = null;
  if (personalDetails?.permanentAddressId) {
    permanentAddress = await prisma.address.update({
      data: {
        districtId: body?.permanentDistrictId,
        municipalityVDC: body?.permanentMunicipalityVDC,
        ward: body?.permanentWard,
        streetAddress: body?.permanentStreetAddress,
        latlng: body?.permanentLatLng,
      },
      where: {
        id: personalDetails?.permanentAddressId,
      },
    });
  } else {
    permanentAddress = await prisma.address.create({
      data: {
        districtId: body?.permanentDistrictId,
        municipalityVDC: body?.permanentMunicipalityVDC,
        ward: body?.permanentWard,
        streetAddress: body?.permanentStreetAddress,
        latlng: body?.permanentLatLng,
      },
    });
  }

  let currentAddress = null;
  const directoryPath = path.join(process.cwd(), "public", "static_map");
  const files = await readdir(directoryPath);

  // Define date range
  const startDate = moment().startOf("month");
  const endDate = moment().endOf("month");

  // Filter the files
  const filteredFiles = [];

  for (const file of files) {
    // Check if the file name starts with 'google'
    if (file.startsWith("google")) {
      // Get file stats (to get the creation/modification time)
      const data = file.split("-");

      // Check if the file modification date is within the specified range
      if (
        moment(data[1], "YYMMDDHHmmss") >= startDate &&
        moment(data[1], "YYMMDDHHmmss") < endDate
      ) {
        filteredFiles.push(file);
      }
    }
  }
  let mapUrlType = "google";

  if (filteredFiles?.length < 75000) {
    mapUrlType = "google";
  } else {
    mapUrlType = "here";
  }
  const imgRes = await fetch(
    mapUrlType == "google"
      ? "https://maps.googleapis.com/maps/api/staticmap?size=905x465&maptype=terrain&center=27.674422763463884,85.32339093995321&zoom=18&markers=color:black%7Clabel:H%7C27.674422763463884,85.32339093995321&key=AIzaSyAKLUx_rnltQ2u9Xr39DcpX3UdRr293gCU&style=feature:all|element:all|saturation:-100&style=feature:poi.business|visibility:on&style=feature:poi.government|visibility:on&style=feature:poi.medical|visibility:on&style=feature:poi.school|visibility:on&style=feature:poi.sports_complex|visibility:on&style=feature:poi.park|visibility:on&style=feature:poi.place_of_worship|visibility:on&style=feature:poi.attraction|visibility:on&format=jpg"
      : "https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=3L4BmwldjcjS0CCrEG-A8Fj7QyNuhJwg5ebKJHDUHnI&c=27.67440%2C85.32343&z=18&w=700&h=600&poix0=27.67440%2C85.32343%3Bred%3Bwhite%3B1"
  );
  if (!imgRes.ok) {
    throw new Error("Network response was not ok");
  }
  console.log("imgResimgRes", imgRes);
  const arrayBuffer = await imgRes.arrayBuffer();
  const buffer = Buffer.from(arrayBuffer);
  // Define the path where the image will be saved
  const fileNameWithExt =
    mapUrlType +
    "-" +
    moment().format("YYMMDDHHmmss") +
    "-" +
    user?.id +
    ".jpeg";
  const filePath = path.join(
    process.cwd(),
    "public",
    "static_map",
    fileNameWithExt
  );
  // Ensure the directory exists
  fs.mkdirSync(path.dirname(filePath), { recursive: true });
  // Write the image to the file system
  fs.writeFileSync(filePath, buffer);
  if (personalDetails?.currentAddressId) {
    currentAddress = await prisma.address.update({
      data: {
        districtId: body?.currentDistrictId,
        municipalityVDC: body?.currentMunicipalityVDC,
        ward: body?.currentWard,
        streetAddress: body?.currentStreetAddress,
        latlng: body?.currentLatLng,
        static_map_path: fileNameWithExt,
      },
      where: {
        id: personalDetails?.currentAddressId,
      },
    });
  } else {
    currentAddress = await prisma.address.create({
      data: {
        districtId: body?.currentDistrictId,
        municipalityVDC: body?.currentMunicipalityVDC,
        ward: body?.currentWard,
        streetAddress: body?.currentStreetAddress,
        latlng: body?.currentLatLng,
        static_map_path: fileNameWithExt,
      },
    });
  }
  personalDetails = await prisma.personalDetails.update({
    data: {
      permanentAddressId: permanentAddress.id,
      currentAddressId: currentAddress.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
