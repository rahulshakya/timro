import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
/**
 *
 * Gets the list of agent so that user can link with one of them
 */
export async function GET(
  request,
  // : Request
  { params }
) {
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    const prisma = new PrismaClient();
    const agents = await prisma.agent.findMany({
      include: { userAgent: true },
    });
    return NextResponse.json({
      data: agents,
      message: "Get Agent Successfully",
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
/**
 *
 * Links agent with a user's personal details
 */
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    console.log("datat", user);
    // const userData = await prisma.user.findFirst({
    //   where: {
    //     id: user.id,
    //   },
    // });

    const personalDetails = await prisma.personalDetails.update({
      data: {
        agentId: body.agentId,
      },
      where: {
        id: user.personalDetailsId,
      },
      include: include_personalDetails,
    });

    return NextResponse.json({
      data: personalDetails,
      message: "User updated successfully",
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
