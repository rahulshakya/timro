import { NextResponse } from "next/server";
import { HealthStatus, PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log("datat", user);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const familyHistory = await prisma.familyHistory.create({
    data: {
      motherDOB: body?.motherDOB,
      motherHealth: body?.motherHealth,
      motherDemiseReason: body?.motherDemiseReason,
      motherDemiseYear: body?.motherDemiseYear,
      fatherDOB: body?.fatherDOB,
      fatherHealth: body?.fatherHealth,
      fatherDemiseReason: body?.fatherDemiseReason,
      fatherDemiseYear: body?.fatherDemiseYear,
      brotherDOB: body?.brotherDOB,
      brotherHealth: body?.brotherHealth,
      brotherDemiseReason: body?.brotherDemiseReason,
      brotherDemiseYear: body?.brotherDemiseYear,
      sisterDOB: body?.sisterDOB,
      sisterHealth: body?.sisterHealth,
      sisterDemiseReason: body?.sisterDemiseReason,
      sisterDemiseYear: body?.sisterDemiseYear,
      spouseDOB: body?.spouseDOB,
      spouseHealth: body?.spouseHealth,
      spouseDemiseReason: body?.spouseDemiseReason,
      spouseDemiseYear: body?.spouseDemiseYear,
      childrenDOB: body?.childrenDOB,
      childrenHealth: body?.childrenHealth,
      childrenDemiseReason: body?.childrenDemiseReason,
      childrenDemiseYear: body?.childrenDemiseYear,
    },
  });

  const personalDetails = await prisma.personalDetails.update({
    data: {
      familyHistoryId: familyHistory.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
