import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  if (accessToken && user) {
    console.log("datat", user);
    // const userData = await prisma.user.findFirst({
    //   where: {
    //     id: user.id,
    //   },
    // });
    const document = await prisma.document.create({
      data: {
        refNo: body?.refNo,
        issueFrom: body?.issueFrom,
        issueDate: body?.issueDate,
      },
    });

    return NextResponse.json({
      data: document,
      message: "User updated successfully",
      meta_data: {},
    });
  } else {
    return NextResponse.json(
      {
        message: "Un-authorized",
        data: "Not authorized, please recheck your email and password",
        meta_data: {},
      },
      { status: 401 }
    );
  }
}
