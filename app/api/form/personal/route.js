import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function PUT(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const personalDetails = await prisma.personalDetails.findFirst({
    where: {
      id: user?.personalDetailsId,
    },
  });

  const updatedPersonalDetails = await prisma.personalDetails.update({
    data: {
      nameInNepali: body?.nameInNepali,
      surnameInNepali: body?.surnameInNepali,
      maritalStatus: body?.maritalStatus,
      gender: body?.gender,
      nationality: body?.nationality,
      dob: body?.dob,
      telephone: body?.telephone,
      mobile: body?.mobile,
      email: body?.email,
      educationQualification: body?.educationQualification,
      professionBusiness: body?.professionBusiness,
      descDailyTask: body?.descDailyTask,
      nameAddressEmployedOrg: body?.nameAddressEmployedOrg,
      natureEmployedOrg: body?.natureEmployedOrg,
      haveInsuranceApplicationBeenRejected:
        body?.haveInsuranceApplicationBeenRejected,
      govtDocumentId: body?.govtDocumentId,
    },
    where: {
      id: personalDetails?.id,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: updatedPersonalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
