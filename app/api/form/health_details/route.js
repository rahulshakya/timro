import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import { include_personalDetails } from "../address/route";
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  console.log("datat", user);
  // const userData = await prisma.user.findFirst({
  //   where: {
  //     id: user.id,
  //   },
  // });
  const healthDetail = await prisma.healthDetail.create({
    data: {
      areYouCompletelyHealthyNow: body?.areYouCompletelyHealthyNow,
      areYouPregnantNow: body?.areYouPregnantNow,
      lastDeliveryDate: body?.lastDeliveryDate,
      wasItCSection: body?.wasItCSection,
      wasTreatmentInLast5YearsIfSoNameAddress:
        body?.wasTreatmentInLast5YearsIfSoNameAddress,
      wasTreatmentInLast5YearsMentionMedicalCondition:
        body?.wasTreatmentInLast5YearsMentionMedicalCondition,
      dementiaEpilepsyRheumatismAsthmaTuberculosisCancerEpilepsyDiabetesHemophilia:
        body?.dementiaEpilepsyRheumatismAsthmaTuberculosisCancerEpilepsyDiabetesHemophilia,
      livedPersonSufferingCommunicableDiseaseThreeYears:
        body?.livedPersonSufferingCommunicableDiseaseThreeYears,
      dizzinessRingingConvulsionsFitsNeurastheniaNeuralgiaParalysisFainting:
        body?.dizzinessRingingConvulsionsFitsNeurastheniaNeuralgiaParalysisFainting,
      coughAsthmaPneumoniaPleurisyHaemorrhageTuberculosisLungDisease:
        body?.coughAsthmaPneumoniaPleurisyHaemorrhageTuberculosisLungDisease,
      faintingChestPainSuffocationHeartPalpitationsOtherHeartDiseaseBloodPressure:
        body?.faintingChestPainSuffocationHeartPalpitationsOtherHeartDiseaseBloodPressure,
      sprueCholeraAnemiaColicAppendicitisAnyStomachLiverFecalIntestinalDisease:
        body?.sprueCholeraAnemiaColicAppendicitisAnyStomachLiverFecalIntestinalDisease,
      anyDiseaseRelatedSkin: body?.anyDiseaseRelatedSkin,
      herniaHydroceleVaricoceleFistulaVeinSwellingDisease:
        body?.herniaHydroceleVaricoceleFistulaVeinSwellingDisease,
      kidneyBladderWaterPlanetRheumatismMetalRingwormSexuallyRelatedDisease:
        body?.kidneyBladderWaterPlanetRheumatismMetalRingwormSexuallyRelatedDisease,
      cancerDisease: body?.cancerDisease,
      anyDiseaseEarNoseThroatEye: body?.anyDiseaseEarNoseThroatEye,
      measlesSannipat: body?.measlesSannipat,
      smallPox: body?.smallPox,
      whenWasBloodPusSugarAlbuminSeen: body?.whenWasBloodPusSugarAlbuminSeen,
      xraysElectrocardiographsBloodTestsDone:
        body?.xraysElectrocardiographsBloodTestsDone,
      anySurgery: body?.anySurgery,
      hivAidsRelated: body?.hivAidsRelated,
    },
  });

  const personalDetails = await prisma.personalDetails.update({
    data: {
      healthDetailId: healthDetail.id,
    },
    where: {
      id: user.personalDetailsId,
    },
    include: include_personalDetails,
  });

  return NextResponse.json({
    data: personalDetails,
    message: "User updated successfully",
    meta_data: {},
  });
}
