import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import {
  startOfWeek,
  endOfWeek,
  startOfMonth,
  endOfMonth,
  subMonths,
} from "date-fns";

export async function GET(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  // const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  try {
    const twentyFourHoursAgo = new Date();
    twentyFourHoursAgo.setMinutes(twentyFourHoursAgo.getMinutes() - 5);
    await prisma.history.updateMany({
      data: {
        status: "TRYABLE",
      },
      where: {
        OR: [{ status: "IN_PROGRESS" }, { status: "CREATED" }],
        AND: {
          updatedAt: {
            lt: twentyFourHoursAgo, // Records where updatedAt is less than 24 hours ago
          },
        },
      },
    });
    if (accessToken && user) {
      await prisma.history.updateMany({
        data: {
          status: "TRYABLE",
        },
        where: {
          OR: [{ status: "IN_PROGRESS" }, { status: "CREATED" }],
          AND: {
            updatedAt: {
              lt: twentyFourHoursAgo, // Records where updatedAt is less than 24 hours ago
            },
          },
        },
      });
      await prisma.history.updateMany({
        data: {
          status: "REJECTED",
        },
        where: {
          OR: [{ status: "TRYING" }, { status: "TRYABLE" }],
          AND: {
            updatedAt: {
              lt: twentyFourHoursAgo, // Records where updatedAt is less than 24 hours ago
            },
          },
        },
      });
      const historyAll = await prisma.history.findMany({
        where: {
          OR: [
            { status: "CREATED" },
            { status: "IN_PROGRESS" },
            { status: "TRYABLE" },
            { status: "TRYING" },
            // { status: "REJECTED" },
          ],
        },
        include: {
          plan: true,
          user: { include: { personalDetails: true } },
        },
        orderBy: {
          updatedAt: "desc",
        },
      });
      const today = new Date();
      const startOfThisWeek = startOfWeek(today);
      const endOfThisWeek = endOfWeek(today);

      // Get start and end of last month
      const startOfThisMonth = startOfMonth(today);
      const endOfThisMonth = endOfMonth(today);
      const thisWeekCount = await prisma.history.count({
        where: {
          updatedAt: {
            gte: startOfThisWeek,
            lte: endOfThisWeek,
          },
        },
      });

      // Fetch records for last month
      const thisMonthCount = await prisma.history.count({
        where: {
          updatedAt: {
            gte: startOfThisMonth,
            lte: endOfThisMonth,
          },
        },
      });
      return NextResponse.json({
        data: historyAll,
        message: "Leads listed successfully",
        meta_data: {
          startOfThisWeek,
          endOfThisWeek,
          startOfThisMonth,
          endOfThisMonth,

          thisWeekCount,
          thisMonthCount,
        },
      });
    } else {
      return NextResponse.json(
        {
          message: "Un-authorized",
          data: "Not authorized, please recheck your email and password",
          meta_data: {},
        },
        { status: 401 }
      );
    }
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { message: "Error", error: error.message },
      { status: 500 }
    );
  }
}
