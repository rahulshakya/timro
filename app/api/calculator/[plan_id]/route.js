"use server";
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
const crypto = require("crypto");

function encrypt(text) {
  const password = "TEST123";
  const key = crypto.scryptSync(password, "salt", 32);
  const iv = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  let encrypted = cipher.update(text, "utf8", "hex");
  encrypted += cipher.final("hex");

  return { iv: iv.toString("hex"), formula: encrypted };
}
export async function GET(
  request,
  // : Request
  { params }
) {
  // : { params: { company_id: number; plan_id: number } }
  // : { params: { company_id: number } }
  const prisma = new PrismaClient();
  const data = await prisma.plan.findFirst({
    where: {
      id: params.plan_id,
    },
    include: {
      calculate: true,
      inputs: {
        include: { options: true },
      },
      category: true,
      company: true,
    },
  });

  let encryptedData = encrypt(JSON.stringify(data?.calculate));

  return NextResponse.json({
    message: "Calculator fetched successfully",
    data: { ...data, calculate: encryptedData.formula },
    meta_data: { vi: encryptedData.iv, path: "calculate" },
  });
}
