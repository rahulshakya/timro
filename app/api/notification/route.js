import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
export async function GET(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  // const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  try {
    if (accessToken && user) {
      console.log(":Asdasdasd", user);
      const notification = await prisma.notification.findMany({
        where: {
          createdId: user.id,
        },
        orderBy: {
          createdAt: "desc",
        },
      });
      return NextResponse.json({
        data: notification,
        message: "Notification listed successfully",
        meta_data: {},
      });
    } else {
      return NextResponse.json(
        {
          message: "Un-authorized",
          data: "Not authorized, please recheck your email and password",
          meta_data: {},
        },
        { status: 401 }
      );
    }
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { message: "Error", error: error.message },
      { status: 500 }
    );
  }
}
