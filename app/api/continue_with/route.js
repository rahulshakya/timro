import { signJwtAccessToken } from "@/lib/jwt";
import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";
export async function POST(request) {
  const body = await request.json();
  const prisma = new PrismaClient();
  let user = await prisma.user.findFirst({
    where: {
      email: body.email,
      identifier: body.identifier,
      status: "ACTIVATED",
    },
  });
  if (user) {
  } else {
    if (body?.account) {
      //is valid user google/apple user
      user = await prisma.user.create({
        data: {
          email: body?.email,
          identifier: body?.identifier,
          firstName: body?.firstName,
          lastName: body?.lastName,
          fcm: body?.fcm || null,
        },
      });
    } else {
      return NextResponse.json({
        data: {},
        message: "Something went wrong",
        meta_data: {},
      });
    }
  }

  const { identifier, ...jwtUser } = user;
  const accessToken = signJwtAccessToken(jwtUser);
  return NextResponse.json({
    data: jwtUser,
    message: "Authenticated",
    meta_data: {
      access_token: accessToken,
    },
  });
}
