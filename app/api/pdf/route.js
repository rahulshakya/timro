import { PDFDocument, rgb } from "pdf-lib";
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";
import { verifyJwt } from "@/lib/jwt";
import moment from "moment";
import path from "path";
import fs from "fs";
import fontkit from "@pdf-lib/fontkit";
import { data } from "autoprefixer";
import { include_personalDetails } from "../form/address/route";
function getValueFromPath(obj, path) {
  return path
    .split(".")
    .reduce(
      (acc, part) => (acc && acc[part] !== undefined ? acc[part] : null),
      obj
    );
}
function hexToNormalizedRGB(hex) {
  // Remove the '#' if present
  hex = hex.replace(/^#/, "");

  // Parse the hex string into RGB values
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  // Normalize each RGB value to the range (1, 1, 1) by dividing by 255
  const normalizedR = r / 255;
  const normalizedG = g / 255;
  const normalizedB = b / 255;

  return rgb(normalizedR, normalizedG, normalizedB);
}
const theDataFormatter = (data, obj) => {
  console.log(data.data, "----herere");
  const theData = getValueFromPath(obj, data.data);
  console.log(theData, "---data");
  if (data.type == "string") {
    return theData || "";
  }
  if (data.type == "date") {
    return moment(theData).isValid()
      ? moment(theData).format("YYYY-MM-DD")
      : "";
  }
  if (data.type == "age") {
    return moment(theData).isValid() ? moment(theData).fromNow() : "";
  }
  if (data.type == "boolean") {
    return theData ? "Yes" : "No";
  }
  if (data.type.indexOf("==") != -1) {
    return data.type.indexOf(theData) != -1 ? "*" : "";
  }
  return data.data;
};
export async function POST(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  let personalDetails = {};
  if (body?.pdfId) {
    let pdf = await prisma.pdf.findFirst({
      where: {
        id: body?.pdfId,
      },
    });
    personalDetails = pdf.userJson;
  } else {
    personalDetails = await prisma.personalDetails.findFirst({
      where: {
        id: user?.personalDetailsId,
      },
      include: include_personalDetails,
    });
  }
  const history = await prisma.history.findFirst({
    where: {
      id: body?.history,
    },
    include: {
      plan: {
        include: {
          forms: {
            include: {
              map: true,
            },
          },
        },
      },
      user: true,
    },
  });
  console.log(personalDetails, "personalDetailspersonalDetails");
  try {
    const fontColor = hexToNormalizedRGB(body?.fontColor);
    const combinedPdfDoc = await PDFDocument.create();
    console.log(history?.plan?.forms);
    combinedPdfDoc.registerFontkit(fontkit);
    const fontPath = path.resolve("./public/fonts/Preeti.otf"); // Update with your font path
    const fontBytes = fs.readFileSync(fontPath);
    const customFont = await combinedPdfDoc.embedFont(fontBytes);
    const generatedJSON = JSON.parse(history?.generated);
    const obj = { personalDetails, generatedJSON, history };
    for (const pdfUrl of Object.values(history?.plan?.forms)) {
      // Fetch the PDF from the URL as an array buffer
      // const response = await axios.get(pdfUrl, {
      //   responseType: "arraybuffer",
      // });
      const pdfBytes = await fs.promises.readFile(pdfUrl?.path);

      // Load the current PDF
      const currentPdfDoc = await PDFDocument.load(pdfBytes);
      const pages = currentPdfDoc.getPages();
      for (const data of pdfUrl.map) {
        console.log(data?.data, "----herer12312", data?.type);
        if (data?.type == "image") {
          const imagePath = path.join(
            process.cwd(),
            "public",
            "static_map",
            getValueFromPath(obj, data?.data)
          );
          console.log(imagePath, "-----image path");
          const imageBytes = fs.readFileSync(imagePath);
          const jpgImage = await currentPdfDoc.embedJpg(imageBytes);
          const { width, height } = jpgImage.scale(1);
          await pages[0].drawImage(jpgImage, {
            x: 366 + 5,
            y: 353 - 100, // Positioning the image
            width: 181,
            height: 93,
            opacity: 1,
          });
        } else {
          pages[data.page].drawText(theDataFormatter(data, obj), {
            x: data.x,
            y: data.y,
            size: data.size,
            color: fontColor,
          });
        }
      }
      // Copy all pages from the current PDF into the combined PDF
      const editedPages = await combinedPdfDoc.copyPages(
        currentPdfDoc,
        currentPdfDoc.getPageIndices()
      );
      editedPages.forEach((page) => combinedPdfDoc.addPage(page));
    }
    const combinedPdfBytes = await combinedPdfDoc.save();

    // if (generatedJSON.adb > 0) {
    //   secondPage.drawText("*", {
    //     x: 174 - 4, // Default X position if not provided
    //     y: 709 - 4, // Default Y position if not provided
    //     size: 8,
    //     color: fontColor, // Black text
    //   });
    // }
    // if (generatedJSON.mode > 0) {
    //   secondPage.drawText("*", {
    //     x: 174 - 4, // Default X position if not provided
    //     y: 709 - 4, // Default Y position if not provided
    //     size: 8,
    //     color: fontColor, // Black text
    //   });
    // }
    // Serialize the PDF to bytes
    if (!body?.pdfId) {
      await prisma.pdf.create({
        data: {
          userJson: personalDetails,
          historyId: history.id,
        },
      });
    }

    const responseHeaders = new Headers({
      "Content-Type": "application/pdf",
      "Content-Disposition": "attachment; filename=modified.pdf",
    });

    return new Response(Buffer.from(combinedPdfBytes), {
      headers: responseHeaders,
    });
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { message: "Error modifying the PDF", error: error.message },
      { status: 500 }
    );
  }
}
export async function GET(
  request,
  // : Request
  { params }
) {
  const prisma = new PrismaClient();
  // const body = await request.json();
  const accessToken = request.headers.get("Authorization");
  const user = verifyJwt(accessToken);
  try {
    if (accessToken && user) {
      const pdfs = await prisma.pdf.findMany();

      return NextResponse.json({
        data: pdfs,
        message: "PDFs listed successfully",
        meta_data: {},
      });
    } else {
      return NextResponse.json(
        {
          message: "Un-authorized",
          data: "Not authorized, please recheck your email and password",
          meta_data: {},
        },
        { status: 401 }
      );
    }
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      { message: "Error modifying the PDF", error: error.message },
      { status: 500 }
    );
  }
}

// {\"chart\":{\"termLife\":{\"18\":{\"5\":1.46,\"6\":1.46,\"7\":1.46,\"8\":1.46,\"9\":1.47,\"10\":1.47,\"11\":1.47,\"12\":1.47,\"13\":1.47,\"14\":1.47,\"15\":1.47,\"16\":1.48,\"17\":1.48,\"18\":1.49,\"19\":1.51,\"20\":1.52,\"21\":1.54,\"22\":1.56,\"23\":1.59,\"24\":1.62,\"25\":1.65,\"26\":1.69,\"27\":1.72,\"28\":1.76,\"29\":1.81,\"30\":1.85},
// \"19\":{\"5\":1.52,\"6\":1.51,\"7\":1.51,\"8\":1.51,\"9\":1.51,\"10\":1.51,\"11\":1.51,\"12\":1.51,\"13\":1.5,\"14\":1.51,\"15\":1.51,\"16\":1.52,\"17\":1.53,\"18\":1.55,\"19\":1.57,\"20\":1.6,\"21\":1.59,\"22\":1.62,\"23\":1.65,\"24\":1.65,\"25\":1.72,\"26\":1.76,\"27\":1.8,\"28\":1.85,\"29\":1.9,\"30\":1.96},
// \"20\":{\"5\":1.57,\"6\":1.55,\"7\":1.55,\"8\":1.55,\"9\":1.55,\"10\":1.55,\"11\":1.54,\"12\":1.53,\"13\":1.53,\"14\":1.53,\"15\":1.54,\"16\":1.55,\"17\":1.56,\"18\":1.58,\"19\":1.6,\"20\":1.62,\"21\":1.65,\"22\":1.65,\"23\":1.72,\"24\":1.73,\"25\":1.8,\"26\":1.85,\"27\":1.85,\"28\":1.95,\"29\":2.01,\"30\":2.07},
// \"21\":{\"5\":1.61,\"6\":1.59,\"7\":1.59,\"8\":1.58,\"9\":1.58,\"10\":1.58,\"11\":1.57,\"12\":1.56,\"13\":1.56,\"14\":1.57,\"15\":1.57,\"16\":1.59,\"17\":1.61,\"18\":1.63,\"19\":1.65,\"20\":1.68,\"21\":1.72,\"22\":1.76,\"23\":1.8,\"24\":1.84,\"25\":1.89,\"26\":1.94,\"27\":1.99,\"28\":2.06,\"29\":2.12,\"30\":2.19},
// \"22\":{\"5\":1.64,\"6\":1.63,\"7\":1.62,\"8\":1.61,\"9\":1.6,\"10\":1.6,\"11\":1.59,\"12\":1.59,\"13\":1.59,\"14\":1.6,\"15\":1.61,\"16\":1.63,\"17\":1.65,\"18\":1.68,\"19\":1.71,\"20\":1.75,\"21\":1.79,\"22\":1.83,\"23\":1.88,\"24\":1.93,\"25\":1.98,\"26\":2.04,\"27\":2.11,\"28\":2.18,\"29\":2.25,\"30\":2.33},
// \"23\":{\"5\":1.68,\"6\":1.65,\"7\":1.64,\"8\":1.63,\"9\":1.62,\"10\":1.62,\"11\":1.62,\"12\":1.62,\"13\":1.62,\"14\":1.64,\"15\":1.66,\"16\":1.68,\"17\":1.71,\"18\":1.74,\"19\":1.78,\"20\":1.83,\"21\":1.87,\"22\":1.92,\"23\":1.97,\"24\":2.03,\"25\":2.09,\"26\":2.16,\"27\":2.23,\"28\":2.31,\"29\":2.4,\"30\":2.45},
// \"24\":{\"5\":1.7,\"6\":1.68,\"7\":1.66,\"8\":1.65,\"9\":1.64,\"10\":1.64,\"11\":1.64,\"12\":1.65,\"13\":1.66,\"14\":1.68,\"15\":1.71,\"16\":1.74,\"17\":1.77,\"18\":1.82,\"19\":1.86,\"20\":1.91,\"21\":1.96,\"22\":2.01,\"23\":2.08,\"24\":2.14,\"25\":2.21,\"26\":2.29,\"27\":2.38,\"28\":2.47,\"29\":2.56,\"30\":2.66},
// \"25\":{\"5\":1.72,\"6\":1.65,\"7\":1.67,\"8\":1.66,\"9\":1.66,\"10\":1.67,\"11\":1.68,\"12\":1.69,\"13\":1.71,\"14\":1.74,\"15\":1.77,\"16\":1.81,\"17\":1.85,\"18\":1.9,\"19\":1.95,\"20\":2,\"21\":2.06,\"22\":2.13,\"23\":2.2,\"24\":2.27,\"25\":2.35,\"26\":2.44,\"27\":2.54,\"28\":2.64,\"29\":2.74,\"30\":2.85},
// \"26\":{\"5\":1.73,\"6\":1.7,\"7\":1.69,\"8\":1.68,\"9\":1.69,\"10\":1.7,\"11\":1.72,\"12\":1.74,\"13\":1.77,\"14\":1.8,\"15\":1.84,\"16\":1.89,\"17\":1.94,\"18\":1.99,\"19\":2.05,\"20\":2.11,\"21\":2.18,\"22\":2.25,\"23\":2.33,\"24\":2.42,\"25\":2.51,\"26\":2.62,\"27\":2.72,\"28\":2.83,\"29\":2.95,\"30\":3.07},
// \"27\":{\"5\":1.74,\"6\":1.72,\"7\":1.71,\"8\":1.71,\"9\":1.73,\"10\":1.75,\"11\":1.77,\"12\":1.8,\"13\":1.83,\"14\":1.88,\"15\":1.93,\"16\":1.98,\"17\":2.04,\"18\":2.1,\"19\":2.16,\"20\":2.24,\"21\":2.31,\"22\":2.4,\"23\":2.49,\"24\":2.59,\"25\":2.7,\"26\":2.81,\"27\":2.93,\"28\":3.05,\"29\":3.18,\"30\":3.32},
// \"28\":{\"5\":1.75,\"6\":1.74,\"7\":1.74,\"8\":1.75,\"9\":1.77,\"10\":1.8,\"11\":1.83,\"12\":1.97,\"13\":1.92,\"14\":1.97,\"15\":2.03,\"16\":2.09,\"17\":2.15,\"18\":2.22,\"19\":2.3,\"20\":2.38,\"21\":2.47,\"22\":2.57,\"23\":2.68,\"24\":2.75,\"25\":2.91,\"26\":3.03,\"27\":3.16,\"28\":3.3,\"29\":3.44,\"30\":3.59},
// \"29\":{\"5\":1.78,\"6\":1.77,\"7\":1.78,\"8\":1.81,\"9\":1.84,\"10\":1.88,\"11\":1.92,\"12\":1.97,\"13\":2.02,\"14\":2.08,\"15\":2.15,\"16\":2.21,\"17\":2.29,\"18\":2.37,\"19\":2.46,\"20\":2.55,\"21\":2.66,\"22\":2.77,\"23\":2.85,\"24\":3.01,\"25\":3.14,\"26\":3.28,\"27\":3.43,\"28\":3.58,\"29\":3.73,\"30\":3.88},
// \"30\":{\"5\":1.82,\"6\":1.82,\"7\":1.85,\"8\":1.88,\"9\":1.92,\"10\":1.97,\"11\":2.02,\"12\":2.08,\"13\":2.15,\"14\":2.21,\"15\":2.28,\"16\":2.36,\"17\":2.45,\"18\":2.54,\"19\":2.64,\"20\":2.75,\"21\":2.87,\"22\":3,\"23\":3.13,\"24\":3.27,\"25\":3.41,\"26\":3.57,\"27\":3.72,\"28\":3.88,\"29\":4.05,\"30\":4.21},
// \"31\":{\"5\":1.88,\"6\":1.9,\"7\":1.93,\"8\":1.98,\"9\":2.03,\"10\":2.09,\"11\":2.15,\"12\":2.22,\"13\":2.29,\"14\":2.36,\"15\":2.45,\"16\":2.54,\"17\":2.64,\"18\":2.74,\"19\":2.86,\"20\":2.98,\"21\":3.12,\"22\":3.26,\"23\":3.4,\"24\":3.56,\"25\":3.72,\"26\":3.88,\"27\":4.05,\"28\":4.22,\"29\":4.4,\"30\":4.58},
// \"32\":{\"5\":1.98,\"6\":2.01,\"7\":2.05,\"8\":2.1,\"9\":2.17,\"10\":2.24,\"11\":2.31,\"12\":2.38,\"13\":2.46,\"14\":2.55,\"15\":2.64,\"16\":2.74,\"17\":2.86,\"18\":2.98,\"19\":3.11,\"20\":3.25,\"21\":3.4,\"22\":3.55,\"23\":3.72,\"24\":3.88,\"25\":4.06,\"26\":4.24,\"27\":4.42,\"28\":4.6,\"29\":4.75,\"30\":4.99},
// \"33\":{\"5\":2.1,\"6\":2.14,\"7\":2.19,\"8\":2.26,\"9\":2.33,\"10\":2.41,\"11\":2.45,\"12\":2.57,\"13\":2.66,\"14\":2.76,\"15\":2.86,\"16\":2.98,\"17\":3.11,\"18\":3.25,\"19\":3.4,\"20\":3.55,\"21\":3.72,\"22\":3.89,\"23\":4.07,\"24\":4.25,\"25\":4.44,\"26\":4.63,\"27\":4.82,\"28\":5.02,\"29\":5.23,\"30\":5.44},
// \"34\":{\"5\":2.25,\"6\":2.29,\"7\":2.36,\"8\":2.44,\"9\":2.52,\"10\":2.61,\"11\":2.69,\"12\":2.78,\"13\":1.88,\"14\":3,\"15\":3.12,\"16\":3.26,\"17\":3.4,\"18\":3.56,\"19\":3.72,\"20\":3.85,\"21\":4.07,\"22\":4.26,\"23\":4.45,\"24\":4.65,\"25\":4.85,\"26\":5.06,\"27\":5.26,\"28\":5.48,\"29\":5.71,\"30\":5.94},
// \"35\":{\"5\":2.42,\"6\":2.48,\"7\":2.56,\"8\":2.64,\"9\":2.73,\"10\":2.83,\"11\":2.92,\"12\":3.02,\"13\":3.14,\"14\":3.27,\"15\":3.41,\"16\":3.57,\"17\":3.73,\"18\":3.9,\"19\":4.08,\"20\":4.27,\"21\":4.47,\"22\":4.68,\"23\":4.88,\"24\":5.09,\"25\":5.31,\"26\":5.53,\"27\":5.76,\"28\":5.99,\"29\":6.24,\"30\":6.5},
// \"36\":{\"5\":2.63,\"6\":2.7,\"7\":2.78,\"8\":2.87,\"9\":2.96,\"10\":3.07,\"11\":3.18,\"12\":3.3,\"13\":3.44,\"14\":3.59,\"15\":3.75,\"16\":3.92,\"17\":4.1,\"18\":4.25,\"19\":4.45,\"20\":4.7,\"21\":4.91,\"22\":5.13,\"23\":5.35,\"24\":5.58,\"25\":5.81,\"26\":6.05,\"27\":6.3,\"28\":6.56,\"29\":6.83,\"30\":7.11},
// \"37\":{\"5\":2.87,\"6\":2.94,\"7\":3.02,\"8\":3.12,\"9\":3.23,\"10\":3.35,\"11\":3.48,\"12\":3.62,\"13\":3.77,\"14\":3.94,\"15\":4.12,\"16\":4.31,\"17\":4.51,\"18\":4.72,\"19\":4.94,\"20\":5.16,\"21\":5.39,\"22\":5.63,\"23\":5.86,\"24\":6.1,\"25\":6.36,\"26\":6.62,\"27\":6.9,\"28\":7.18,\"29\":7.48,\"30\":7.77},
// \"38\":{\"5\":3.13,\"6\":3.2,\"7\":3.29,\"8\":3.4,\"9\":3.53,\"10\":3.67,\"11\":3.81,\"12\":3.97,\"13\":4.15,\"14\":4.33,\"15\":4.53,\"16\":4.74,\"17\":4.97,\"18\":5.19,\"19\":5.43,\"20\":5.68,\"21\":5.92,\"22\":6.17,\"23\":6.42,\"24\":6.69,\"25\":6.96,\"26\":7.25,\"27\":7.56,\"28\":7.87,\"29\":8.17,\"30\":8.45},
// \"39\":{\"5\":3.4,\"6\":3.48,\"7\":3.59,\"8\":3.72,\"9\":3.86,\"10\":4.03,\"11\":4.15,\"12\":4.36,\"13\":4.57,\"14\":4.78,\"15\":5,\"16\":5.23,\"17\":5.47,\"18\":5.72,\"19\":5.98,\"20\":6.23,\"21\":6.49,\"22\":6.76,\"23\":7.04,\"24\":7.33,\"25\":7.63,\"26\":7.95,\"27\":8.28,\"28\":8.6,\"29\":8.94,\"30\":9.3},
// \"40\":{\"5\":3.71,\"6\":3.8,\"7\":3.92,\"8\":4.07,\"9\":4.25,\"10\":4.44,\"11\":4.62,\"12\":4.82,\"13\":5.04,\"14\":5.27,\"15\":5.51,\"16\":5.77,\"17\":6.05,\"18\":6.25,\"19\":6.56,\"20\":6.83,\"21\":7.11,\"22\":7.41,\"23\":7.71,\"24\":8.03,\"25\":8.37,\"26\":8.72,\"27\":9.05,\"28\":9.41,\"29\":9.79,\"30\":10.18},
// \"41\":{\"5\":4.04,\"6\":4.15,\"7\":4.3,\"8\":4.47,\"9\":4.67,\"10\":4.89,\"11\":5.1,\"12\":5.32,\"13\":5.56,\"14\":5.81,\"15\":6.08,\"16\":6.35,\"17\":6.63,\"18\":6.91,\"19\":7.2,\"20\":7.49,\"21\":7.8,\"22\":8.12,\"23\":8.46,\"24\":8.81,\"25\":9.18,\"26\":9.53,\"27\":9.91,\"28\":10.31,\"29\":10.72},
// \"42\":{\"5\":4.41,\"6\":4.55,\"7\":4.72,\"8\":4.92,\"9\":5.15,\"10\":5.39,\"11\":5.62,\"12\":5.87,\"13\":6.14,\"14\":6.41,\"15\":6.7,\"16\":6.99,\"17\":7.28,\"18\":7.58,\"19\":7.89,\"20\":8.21,\"21\":8.55,\"22\":8.91,\"23\":9.28,\"24\":9.67,\"25\":10.04,\"26\":10.43,\"27\":10.85,\"28\":11.29},
// \"43\":{\"5\":4.84,\"6\":5.01,\"7\":5.21,\"8\":5.44,\"9\":5.69,\"10\":5.96,\"11\":6.22,\"12\":6.45,\"13\":6.78,\"14\":7.07,\"15\":7.38,\"16\":7.69,\"17\":8,\"18\":8.32,\"19\":8.66,\"20\":9.01,\"21\":9.39,\"22\":9.78,\"23\":10.18,\"24\":10.58,\"25\":10.99,\"26\":11.43,\"27\":11.9},
// \"44\":{\"5\":5.36,\"6\":5.55,\"7\":5.77,\"8\":6.03,\"9\":6.31,\"10\":6.6,\"11\":6.88,\"12\":7.18,\"13\":7.49,\"14\":7.81,\"15\":8.12,\"16\":8.45,\"17\":8.78,\"18\":9.14,\"19\":9.51,\"20\":9.9,\"21\":10.31,\"22\":10.78,\"23\":11.15,\"24\":11.59,\"25\":12.06,\"26\":12.55},
// \"45\":{\"5\":5.95,\"6\":6.16,\"7\":6.41,\"8\":6.65,\"9\":7,\"10\":7.32,\"11\":7.62,\"12\":7.94,\"13\":8.27,\"14\":8.6,\"15\":8.93,\"16\":9.28,\"17\":9.65,\"18\":10.04,\"19\":10.45,\"20\":10.88,\"21\":11.33,\"22\":11.76,\"23\":11.23,\"24\":12.72,\"25\":13.23},
// \"46\":{\"5\":6.61,\"6\":6.85,\"7\":7.13,\"8\":7.43,\"9\":7.76,\"10\":6.11,\"11\":8.43,\"12\":8.77,\"13\":9.1,\"14\":9.45,\"15\":9.81,\"16\":10.2,\"17\":19.6,\"18\":11.03,\"19\":11.49,\"20\":11.96,\"21\":12.41,\"22\":12.9,\"23\":13.42,\"24\":13.96},
// \"47\":{\"5\":7.36,\"6\":7.61,\"7\":7.91,\"8\":8.25,\"9\":8.6,\"10\":8.97,\"11\":9.31,\"12\":9.66,\"13\":10.01,\"14\":10.38,\"15\":10.78,\"16\":11.2,\"17\":11.65,\"18\":12.13,\"19\":12.63,\"20\":13.1,\"21\":13.61,\"22\":14.16,\"23\":14.74},
// \"48\":{\"5\":8.18,\"6\":8.46,\"7\":8.78,\"8\":9.13,\"9\":9.51,\"10\":9.91,\"11\":10.25,\"12\":10.61,\"13\":10.99,\"14\":11.4,\"15\":11.84,\"16\":12.31,\"17\":12.81,\"18\":13.33,\"19\":13.83,\"20\":14.37,\"21\":14.94,\"22\":15.55},
// \"49\":{\"5\":9.08,\"6\":9.38,\"7\":9.72,\"8\":10.1,\"9\":10.5,\"10\":10.85,\"11\":11.25,\"12\":11.64,\"13\":12.06,\"14\":12.52,\"15\":13,\"16\":13.53,\"17\":14.07,\"18\":14.6,\"19\":15.16,\"20\":15.77,\"21\":16.41},
// \"50\":{\"5\":10.06,\"6\":10.37,\"7\":10.74,\"8\":11.13,\"9\":11.53,\"10\":11.95,\"11\":12.34,\"12\":12.76,\"13\":13.23,\"14\":13.74,\"15\":14.28,\"16\":14.86,\"17\":15.4,\"18\":16,\"19\":16.64,\"20\":17.31},
// \"51\":{\"5\":11.12,\"6\":11.45,\"7\":11.83,\"8\":12.21,\"9\":12.63,\"10\":13.08,\"11\":13.52,\"12\":13.99,\"13\":14.52,\"14\":15.09,\"15\":15.69,\"16\":16.25,\"17\":16.88,\"18\":17.55,\"19\":18.27},
// \"52\":{\"5\":12.26,\"6\":12.59,\"7\":12.96,\"8\":13.36,\"9\":13.82,\"10\":14.32,\"11\":14.81,\"12\":15.34,\"13\":15.93,\"14\":16.56,\"15\":17.15,\"16\":19.27,\"17\":18.51,\"18\":17.8},
// \"53\":{\"5\":13.47,\"6\":13.78,\"7\":14.15,\"8\":14.6,\"9\":15.11,\"10\":15.67,\"11\":16.22,\"12\":16.83,\"13\":17.48,\"14\":18.1,\"15\":18.78,\"16\":20.32,\"17\":19.53},
// \"54\":{\"5\":14.71,\"6\":15.02,\"7\":15.44,\"8\":15.95,\"9\":16.52,\"10\":17.17,\"11\":17.79,\"12\":18.46,\"13\":19.1,\"14\":19.81,\"15\":20.6,\"16\":21.44},
// \"55\":{\"5\":16.01,\"6\":16.37,\"7\":16.85,\"8\":17.43,\"9\":18.09,\"10\":18.82,\"11\":19.51,\"12\":20.16,\"13\":20.91,\"14\":21.73,\"15\":22.62},
// \"56\":{\"5\":17.41,\"6\":17.84,\"7\":18.41,\"8\":19.07,\"9\":19.83,\"10\":20.64,\"11\":21.3,\"12\":22.07,\"13\":22.93,\"14\":23.87},
// \"57\":{\"5\":18.56,\"6\":19.48,\"7\":20.14,\"8\":20.91,\"9\":21.75,\"10\":22.53,\"11\":23.32,\"12\":24.22,\"13\":25.2},
// \"58\":{\"5\":20.71,\"6\":21.32,\"7\":22.09,\"8\":22.95,\"9\":23.75,\"10\":24.67,\"11\":25.6,\"12\":26.63},
// \"59\":{\"5\":22.76,\"6\":23.48,\"7\":24.33,\"8\":25.12,\"9\":26.07,\"10\":27.16,\"11\":28.22},
// \"60\":{\"5\":25.13,\"6\":25.93,\"7\":26.67,\"8\":27.63,\"9\":28.75,\"10\":29.99},
// \"61\":{\"5\":27.81,\"6\":28.43,\"7\":29.36,\"8\":30.5,\"9\":31.75},
// \"62\":{\"5\":30.47,\"6\":31.3,\"7\":32.42,\"8\":33.75},
// \"63\":{\"5\":33.53,\"6\":34.57,\"7\":35.9},
// \"64\":{\"5\":37.02,\"6\":38.28},
// \"65\":{\"5\":40.98}},
// \"age_wise_medical\":{\"<10\":{\"<2000000\":\"No Medical Required\",\">2000000\":\"FMR,RUA,ECG\",\">10000000\":\"Refer to Reinsurance\"},
// \">10\":{\"<2000000\":\"No Medical Required\",\">2000000\":\"FMR,RUA,CBC\\nBlood Urea,S. Creatinine\",\">10000000\":\"FMR,CBC,ESR,RUA\\nS. Creatinine,Uric Acid\\nX-Ray,HIV,HBsAG\"},
// \">19\":{\"<2000000\":\"No Medical Required\",\">2000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1\",\">3500000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4\",\">6000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR\",\">10000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR,TMT\"},
// \">46\":{\"<1000000\":\"No Medical Required\",\">1000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\",\">2000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2\",\">3500000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4\",\">6000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR\",\">10000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR,TMT\"},
// \">55\":{\"<500000\":\"No Medical Required\",\">500000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\",\">1500000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2\",\">3500000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR\",\">6000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR,TMT\",\">10000000\":\"FMR,FBS,ECG,RUA\\nCBC,ESR,S. Creatinine\\nM1,M2,M3,M4,CXR,TMT\"}},
// \"professionalAdditionCharges\":{\"Electrician\":[2,0.65],\"Fire Man\":[2.5,1.5],\"Loader\":[2,0.65],\"Painter\":[2,0.65],\"Plumber\":[1,0.65],\"Rafting\":[6,false],\"Mechanics\":[2,0.65],\"Tempo Driver\":[2,0.65],\"Forest Guard\":[2,1.25],\"Others (can be defined as per need)\":[0,0],\"Aircaft maintenance, Engineers in Airlines\":[1.5,1.5],\"Army (Below Major)\":[2.5,2],\"Army Pilot up to Rank of Caption\":[2,0.75],\"Bus and Truck Driver (Heavy Driver)\":[2,0.65],\"Carpenter/Petrol/Attendant/Light Driver\":[1,0.65],\"Cement Factory Helper\":[2.5,0.65],\"Cement Factory Engineers\":[5,0.65],\"Construction and Maintenance\":[2,0.65],\"Mechanical Engineer/Grill Worker\":[1,1],\"Mining and Gass (Worker)\":[2.5,false],\"Non Scheduled Flight\":[5,false],\"Pilot, Steward and Airhostess\":[2,2],\"Police (Below DSP)\":[2,2],\"Rickshaw and Thela Gada\":[2,0.65],\"Trekking Guide\":[6,false],\"Worker of Ship\":[2,2],\"Working by Hanging on Pole (Line Man-NTC)\":[2,0.65],\"Workshop Worker/ Light Driver\":[1,0.65],\"Mining and Natural Resources Technician\":[2,false],\"Hydro and Natural Resources Engineer\":[1,0.65],\"Electrical Engineer\":[1,0.65],\"Adventure Sports Trainer\":[1,0.65],\"Other Sports Trainer\":[1,0.65],\"Macon Workore\":[1,0.65],\"Machine Operators\":[2,1.25],\"Delivery Persons (two wheelers)\":[1,0.65],\"Climbers up to base camp level\":[7,false],\"Tourism Sport other than Rafting and Climbing\":[5,false]}},
// \"age_selected\":18,\"terms\":\"5\",\"coverageAmount\":\"200000\",\"mode\":\"1\",\"adb\":\"0\",\"profession\":\"Rafting\"}"
