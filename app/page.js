"use client";
import Image from "next/image";
import Link from "next/link";

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { AllActions } from "@/redux/allAction";
import { useDispatch } from "react-redux";
import allTypes from "@/redux/allTypes";
import { CONTENT_TYPE } from "@/redux/allApi";
import {
  Sheet,
  SheetClose,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet";
import {
  InputOTP,
  InputOTPGroup,
  InputOTPSeparator,
  InputOTPSlot,
} from "@/components/ui/input-otp";
import { useToast } from "@/hooks/use-toast";
import { ToastAction } from "@/components/ui/toast";

export const description =
  "A login page with two columns. The first column has the login form with email and password. There's a Forgot your passwork link and a link to sign up if you do not have an account. The second column has a cover image.";

export default function Login() {
  const { toast } = useToast();
  const router = useRouter();
  const [formData, setFormData] = useState({
    email: "rahulsaqya@gmail.com",
    fcm: "c2xi4trNMEd9o6oJqoXgbl:APA91bGk5rBNlUTlxjfTXAs0w9MasBs6wrEEdctCV7Tm2XLqCIEfIVkaofihXT7mhMZdKB2DlPGsxDZWxp_880anoETuI5K43ybDTG3eKq1FOYAZrs1gtr_CPJO3bTeQIpWYC8P_x7DI",
    os: "web",
    password: "Qwerty@123",
  });
  const [code, setCode] = useState("");
  const [password, setPassword] = useState("");
  const actions = useDispatch();
  const [showOtp, setShowOtp] = useState(false);
  return (
    <div className="w-full lg:grid lg:min-h-[600px] lg:grid-cols-2 xl:min-h-[800px]">
      <div className="flex items-center justify-center py-12">
        <div className="mx-auto grid w-[350px] gap-6">
          <div className="grid gap-2 text-center">
            <h1 className="text-3xl font-bold">Login</h1>
            <p className="text-balance text-muted-foreground">
              Enter your email below to login to your account
            </p>
          </div>
          <div className="grid gap-4">
            <div className="grid gap-2">
              <Label htmlFor="email">Email</Label>
              <Input
                value={formData?.email}
                onChange={({ target: { value } }) => {
                  setFormData({
                    ...formData,
                    email: value,
                  });
                }}
                id="email"
                type="email"
                placeholder="m@example.com"
                required
              />
            </div>
            <div className="grid gap-2">
              <div className="flex items-center">
                <Label htmlFor="password">Password</Label>
                <Button
                  variant="link"
                  onClick={() => {
                    AllActions(actions, {
                      payload: { email: formData?.email },
                      options: {
                        baseUrl: "http://localhost:3000/api/",
                        method: "post",
                        endPoint: "user/forgot-password",
                        types: allTypes.FORGOT_PASSWORD,
                        bearerToken: false,
                        onSuccess: (res) => {
                          setCode(res?.data?.meta_data?.otp);
                          setShowOtp(true);
                        },
                        onFailure: () => {},
                        contentType: CONTENT_TYPE.JSON,
                      },
                    });
                  }}
                  className="ml-auto inline-block text-sm underline"
                >
                  {" "}
                  Forgot your password?
                </Button>
              </div>
              <Input
                value={formData?.password}
                onChange={({ target: { value } }) => {
                  setFormData({
                    ...formData,
                    password: value,
                  });
                }}
                id="password"
                type="password"
                required
              />
            </div>
            <Button
              onClick={() => {
                AllActions(actions, {
                  payload: formData,
                  options: {
                    baseUrl: "http://localhost:3000/api/",
                    method: "post",
                    endPoint: "user",
                    types: allTypes.LOGIN,
                    bearerToken: true,
                    onSuccess: (res) => {
                      toast({
                        title: res?.data?.message,
                        description: "User login was successfully",
                        action: (
                          <ToastAction altText="Goto schedule to undo">
                            Close
                          </ToastAction>
                        ),
                      });
                      router.push("/agent");
                    },
                    onFailure: (e) => {
                      toast({
                        title: e?.response?.data?.message,
                        description: e?.response?.data?.data,
                        action: (
                          <ToastAction altText="Goto schedule to undo">
                            Close
                          </ToastAction>
                        ),
                      });
                    },
                    contentType: CONTENT_TYPE.JSON,
                  },
                });
              }}
              type="submit"
              className="w-full"
            >
              Login
            </Button>
            <Button variant="outline" className="w-full">
              Login with Google
            </Button>
          </div>
          <div className="mt-4 text-center text-sm">
            Don&apos;t have an account?{" "}
            <Link
              href="https://play.google.com/store/apps/details?id=com.metlife.nepal.business.consumer"
              className="underline"
            >
              Sign up
            </Link>
          </div>
        </div>
      </div>
      <div className="hidden bg-muted lg:block">
        <Image
          src="/placeholder.svg"
          alt="Image"
          width="1920"
          height="1080"
          className="h-full w-full object-cover dark:brightness-[0.2] dark:grayscale"
        />
      </div>
      <Sheet open={showOtp} onOpenChange={setShowOtp}>
        <SheetContent>
          <SheetHeader>
            <SheetTitle>Verify Code</SheetTitle>
            <SheetDescription>
              The OTP code {code} has been send to the email {formData.email}
            </SheetDescription>
          </SheetHeader>
          <div className="grid gap-4 py-4">
            <div className="flex flex-1 flex-col gap-4">
              <Label htmlFor="password">OTP Code</Label>
              <InputOTP maxLength={6}>
                <InputOTPGroup>
                  <InputOTPSlot index={0} />
                  <InputOTPSlot index={1} />
                  <InputOTPSlot index={2} />
                </InputOTPGroup>
                <InputOTPSeparator />
                <InputOTPGroup>
                  <InputOTPSlot index={3} />
                  <InputOTPSlot index={4} />
                  <InputOTPSlot index={5} />
                </InputOTPGroup>
              </InputOTP>
            </div>
            <div className="flex flex-1 flex-col gap-4">
              <Label htmlFor="password">Password</Label>
              <Input
                id="password"
                value={password}
                onChange={(event) => {
                  setPassword(event?.target?.value);
                }}
                className="col-span-3"
              />
            </div>
          </div>
          <SheetFooter>
            <Button
              onClick={() => {
                AllActions(actions, {
                  payload: {
                    new_password: password,
                    otp: code,
                    email: formData?.email,
                  },
                  options: {
                    baseUrl: "http://localhost:3000/api/",
                    method: "post",
                    endPoint: "user/change-password",
                    types: allTypes.CHANGE_PASSWORD,
                    bearerToken: false,
                    onSuccess: (res) => {
                      setShowOtp(false);
                    },
                    onFailure: () => {},
                    contentType: CONTENT_TYPE.JSON,
                  },
                });
              }}
              type="submit"
            >
              Reset Password
            </Button>
          </SheetFooter>
        </SheetContent>
      </Sheet>
    </div>
  );
}
