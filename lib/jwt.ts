import jwt, { JwtPayload } from "jsonwebtoken";

const DEFAULT_SIGIN_OPTION = {
  expiresIn: "1h",
};

export function signJwtAccessToken(payload, options = DEFAULT_SIGIN_OPTION) {
  const secretKey = process.env.SECRET_KEY;
  const token = jwt.sign(payload, secretKey, options);
  return token;
}

export function verifyJwt(token) {
  try {
    const secretKey = process.env.SECRET_KEY;
    const decoded = jwt.verify(token, secretKey);
    return decoded as JwtPayload;
  } catch (error) {
    console.log(error);
  }
}
